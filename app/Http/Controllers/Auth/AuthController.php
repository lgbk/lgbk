<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Models\Auth\AuthToken;

class AuthController extends Controller
{
    
    public function login(Request $request){

        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);

        $user = User::findByEmail($request['email']);

        if(!$user){
            return response('', 401);
        }

        if(!$user->passwordsMatch($request['password'])){
            return response('', 401);
        }

        return response()->json(AuthToken::issuePair($user));
    }

    public function refresh(Request $request){

        $request->validate([
            'token' => 'required'
        ]);

        $token = AuthToken::findRefreshToken($request->token);

        if(!$token || !$token->isValid()){
            return response('', 401);
        }

        $user = $token->user;

        $token->used = true;
        $token->save();

        $sibling = $token->sibling;
        $sibling->used = true;
        $sibling->save();

        return response()->json(AuthToken::issuePair($user));
    }

    public function logout(Request $request){
        $bearer = $request->bearerToken();

        $token = AuthToken::findAuthToken($bearer);
        $token->used = true;
        $token->save();

        $sibling = $token->sibling;
        $sibling->used = true;
        $sibling->save();

        return response('', 200);
    }

    public function unauthenticated(Request $request){
        return response('', 401);
    }

}
