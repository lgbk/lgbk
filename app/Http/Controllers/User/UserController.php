<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class UserController extends Controller
{
    public function register(Request $request){

        $request->validate([
            'email' => 'required',
            'password' => 'required',
            'first_name' => 'required',
            'last_name' => 'required',
        ]);

        $user = User::findByEmail($request->email);

        if($user){
            return response('', 409);
        }

        $user = new User();
        
        $user->email = $request->email;
        $user->password = $request->password;
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        
        $user->save();
        return response()->json($user);
    }

    public function getUser(Request $request){
        $user = Auth::user();

        return response()->json($user);
    }

    public function changePassword(Request $request){
        $user = Auth::user();

        $request->validate([
            'password' => 'required'
        ]);

        $user->password = $request->password;
        $user->save();

        return response('', 204);
    }
    
}
