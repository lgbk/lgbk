<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FaviconController extends Controller
{
    public function faviconIco(Request $request){
        return response()->file(public_path()."/favicon.ico");
    }
}
