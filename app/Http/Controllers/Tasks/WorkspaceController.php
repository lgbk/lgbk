<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Tasks\Workspace;

class WorkspaceController extends Controller
{
    public function createWorkspace(Request $request){
        $user = Auth::user();

        $request->validate([
            'title' => 'required',
            'color' => 'sometimes'
        ]);

        $workspace = new Workspace();
        $workspace->user_id = $user->id;
        $workspace->title = $request->title;
        $workspace->color = $request->color ? $request->color : "4f646f";
        $workspace->order = count(Workspace::allByUser($user)) + 1;

        $workspace->save();

        return response()->json($workspace);
    }

    public function listWorkspaces(Request $request){
        $user = Auth::user();
        return response()->json(Workspace::allByUser($user));
    }

    public function updateWorkspace(Request $request, string $id){
        $user = Auth::user();

        $workspace = Workspace::findById($id);

        if(!$workspace || $workspace->user->id != $user->id){
            return response('', 404);
        }

        if($request->title){
            $workspace->title = $request->title;
        }

        if($request->color){
            $workspace->color = $request->color;
        }

        if($workspace->isDirty()){
            $workspace->save();
        }

        return response()->json($workspace);
    }

    public function reorderWorkspace(Request $request, string $id){
        $user = Auth::user();

        $request->validate([
            'order' => 'required'
        ]);

        $target = Workspace::findById($id);

        if(!$target || $target->user->id != $user->id){
            return response('', 404);
        }

        $target->order = $request->order;
        $target->save();

        foreach(Workspace::allByUser($user) as $workspace){
            if($workspace->id != $target->id){
                if($workspace->order >= $request->order){
                    $workspace->order = $workspace->order + 1;
                    $workspace->save();
                }
            }
        }

        return response()->json(Workspace::allByUser($user));
    }

    public function deleteWorkspace(Request $request, string $id){
        $user = Auth::user();

        $workspace = Workspace::findById($id);

        if(!$workspace || $workspace->user->id != $user->id){
            return response('', 404);
        }

        $workspace->delete();

        return response()->json($workspace);
    }

}
