<?php

namespace App\Http\Controllers\Tasks;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Tasks\Workspace;
use App\Models\Tasks\Task;

class TaskController extends Controller
{

    public function createTask(Request $request){
        $user = Auth::user();

        $request->validate([
            'workspace' => 'required',
            'title' => 'required',
            'priority' => 'sometimes',
            'start_time' => 'sometimes',
            'end_time' => 'sometimes',
            'estimate' => 'sometimes',
            'parent' => 'sometimes'
        ]);

        $workspace = Workspace::findById($request->workspace);

        if(!$workspace || $workspace->user->id != $user->id){
            return response('', 404);
        }

        $task = new Task();
        $task->workspace_id = $workspace->id;
        $task->title = $request->title;

        if($request->priority){
            $task->priority = $request->priority;
        } else {
            $task->priority = 3;
        }

        if($request->start_time){
            $task->start_time = new \DateTime($request->start_time);
            $task->end_time = null;
        }

        if($request->end_time){
            $task->end_time = new \DateTime($request->end_time);
            $task->start_time = null;
        }

        if($request->estimate){
            $task->estimate = $request->estimate;
        }

        $parent = null;
        if($request->parent){
            $parent = Task::findById($request->parent);

            if(!$parent || $parent->workspace->user->id != $user->id){
                return response('', 404);
            }

            $task->parent_id = $parent->id;
        }

        $task->order = count(Task::getSiblingTasks($workspace, $parent)) + 1;

        $task->save();

        return response()->json($task);
    }


    public function getTask(Request $request, ?string $taskId=null){
        $user = Auth::user();

        if($taskId){

            $task = Task::findById($taskId);

            if(!$task || $task->workspace->user->id != $user->id){
                return response('', 404);
            }

            return response()->json($task);

        } else {
            return response()->json(Task::allByUser($user));
        }
    }


    public function getNextTasks(Request $request){
        $user = Auth::user();
        $now = new \DateTime();

        $next = [];

        foreach(Task::allByUser($user) as $task){
            if($task->complete){
                continue;
            }

            if($task->blocked){
                continue;
            }

            if($task->start_time && $now < $task->start_time){
                continue;
            }

            $next[] = $task;
        }

        if(count($next) > 0){
            uasort($next, function($a, $b){
                if ($a->weight == $b->weight) {
                    return 0;
                }
                return ($a->weight > $b->weight) ? -1 : 1;
            });
        }

        return response()->json(collect($next)->values());
    }


    public function updateTask(Request $request, string $taskId){
        $user = Auth::user();

        $request->validate([
            'title' => 'sometimes',
            'priority' => 'sometimes',
            'start_time' => 'sometimes',
            'end_time' => 'sometimes',
            'estimate' => 'sometimes',
            'complete' => 'sometimes',
            'order' => 'sometimes',
            'workspace' => 'sometimes',
            'parent' => 'sometimes',
            'repeat_mode' => 'sometimes',
            'repeat_always' => 'sometimes',
            'repeat_config' => 'sometimes',
        ]);

        $task = Task::findById($taskId);

        if(!$task || $task->workspace->user->id != $user->id){
            return response('', 404);
        }

        if($request->title){
            $task->title = $request->title;
        }

        if($request->priority){
            $task->priority = $request->priority;
        }

        if($request->has("start_time")){
            if($request->start_time){
                $task->start_time = new \DateTime($request->start_time);
                $task->end_time = null;
            } else {
                $task->start_time = null;
            }
        }

        if($request->has("end_time")){
            if($request->end_time){
                $task->end_time = new \DateTime($request->end_time);
                $task->start_time = null;
            } else {
                $task->end_time = null;
            }
        }

        if($request->has("estimate")){
            if($request->estimate > 0){
                $task->estimate = $request->estimate;
            } else {
                $task->estimate = null;
            }
        }

        if($request->has("repeat_mode")){
            $task->repeat_mode = $request->repeat_mode;
        }

        if($request->has("repeat_always")){
            $task->repeat_always = $request->repeat_always;
        }

        if($request->has("repeat_config")){
            $task->repeat_config = $request->repeat_config;
        }

        if($request->has("complete")){
            if($request->complete){
                $task->setCompleted($request->complete);
            } else {
                $task->setCompleted(false);
            }
        }

        if($request->workspace){
            $workspace = Workspace::findById($request->workspace);

            if(!$workspace || $workspace->user->id != $user->id){
                return response('', 404);
            }

            $task->workspace_id = $workspace->id;
        }

        if($request->has("parent")){
            if($request->parent){
                $parent = Task::findById($request->parent);

                if(!$parent || $parent->workspace->user->id != $user->id){
                    return response('', 404);
                }

                $task->parent_id = $parent->id;
            } else {
                $task->parent_id = null;
            }
        }

        if($task->isDirty()){
            $task->save();
        }

        if($request->order || $request->has("parent") || $request->workspace){
            $task = $task->fresh();

            $useWorkspace = $request->workspace ? Workspace::findById($request->workspace) : $task->workspace;
            $useParent = $request->parent ? Task::findById($request->parent) : $task->parent;

            if($request->order){
                $task->order = $request->order;
            } else {
                $task->order = count(Task::getSiblingTasks($useWorkspace, $useParent));
            }

            if($task->isDirty()){
                $task->save();
            }

            if($request->order){
                $target = $task;
    
                foreach(Task::getSiblingTasks($useWorkspace, $useParent) as $task){
                    if($task->id != $target->id){
                        if($task->order >= $request->order){
                            $task->order = $task->order + 1;
                            $task->save();
                        }
                    }
                }
            }

            if($request->workspace){
                foreach($task->children as $child){

                    if($request->workspace){
                        $child->workspace_id = $request->workspace;
                    }

                    if($child->isDirty()){
                        $child->save();
                    }

                }
            }

        }

        return response()->json(Task::allByUser($user));
    }


    public function deleteTask(Request $request, string $taskId){
        $user = Auth::user();

        $task = Task::findById($taskId);
        $workspace = $task->workspace;

        if(!$task || $workspace->user->id != $user->id){
            return response('', 404);
        }

        $task->delete();

        return response()->json(Task::allByUser($user));
    }


    public function addDependencies(Request $request, string $taskId){
        $user = Auth::user();

        $request->validate([
            'dependencies' => 'required'
        ]);

        $target = Task::findById($taskId);

        if(!$target || $target->workspace->user->id != $user->id){
            return response('', 404);
        }

        $dependencies = [];
        foreach($request->dependencies as $id){
            $task = Task::findById($id);

            if(!$task || $task->workspace->user->id != $user->id){
                return response('', 404);
            }

            $dependencies[] = $task;
        }

        foreach($dependencies as $task){
            $target->dependsOn()->attach($task->id);
        }

        return response()->json(Task::allByUser($user));
    }

    public function deleteDependencies(Request $request, string $taskId){
        $user = Auth::user();

        $request->validate([
            'dependencies' => 'required'
        ]);

        $target = Task::findById($taskId);

        if(!$target || $target->workspace->user->id != $user->id){
            return response('', 404);
        }

        $dependencies = [];
        foreach($request->dependencies as $id){
            $task = Task::findById($id);

            if(!$task || $task->workspace->user->id != $user->id){
                return response('', 404);
            }

            $dependencies[] = $task;
        }

        foreach($dependencies as $task){
            $target->dependsOn()->detach($task->id);
        }

        return response()->json(Task::allByUser($user));
    }


    public function updateAllDependencies(Request $request, string $taskId){
        $user = Auth::user();

        $request->validate([
            'dependencies' => 'sometimes'
        ]);

        $target = Task::findById($taskId);

        if(!$target || $target->workspace->user->id != $user->id){
            return response('', 404);
        }

        $dependencies = [];
        foreach($request->dependencies as $id){
            $task = Task::findById($id);

            if(!$task || $task->workspace->user->id != $user->id){
                return response('', 404);
            }

            $dependencies[] = $task;
        }


        //Remove dependencies that are no longer listed
        foreach($target->dependsOn()->get() as $blockingTask){
            if(!in_array($blockingTask->id, $request->dependencies)){
                $target->dependsOn()->detach($blockingTask->id);
            }
        }

        //Attach dependencies that are not currently attached
        foreach($dependencies as $task){

            $alreadyLinked = false;
            foreach($target->dependsOn()->get() as $blockingTask){
                if($task->id == $blockingTask->id){
                    $alreadyLinked = true;
                    break;
                }
            }

            if(!$alreadyLinked){
                $target->dependsOn()->attach($task->id);
            }
            
        }

        return response()->json(Task::allByUser($user));
    }
    
}
