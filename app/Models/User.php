<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\Auth\AuthToken;
use App\Models\Tasks\Workspace;

class User extends Authenticatable
{
    use HasUuids, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
    ];

    protected $guarded = [
        'email_hash',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'email',
        'email_hash',
        'updated_at',
        'created_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'first_name' => 'encrypted',
        'last_name' => 'encrypted',
    ];

    /**
     * Find a user by email address
     * 
     * @param string $email
     * @return User|null
     */
    public static function findByEmail(string $email) {
        return User::where("email_hash", hash('sha256', $email))->first();
    }

    protected function email(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => Crypt::decryptString($value),
            set: fn (string $value) => [
                'email' => Crypt::encryptString($value),
                'email_hash' => hash('sha256', $value),
            ],
        );
    }

    protected function password(): Attribute
    {
        return Attribute::make(
            get: fn (?string $value) => $value,
            set: fn (string $value) => Hash::make($value)
        );
    }

    public function passwordsMatch(string $password){
        return Hash::check($password, $this->password);
    }

    public function authTokens(): HasMany {
        $this->hasMany(AuthToken::class);
    }

    public function workspaces(): HasMany {
        $this->hasMany(Workspace::class);
    }
}
