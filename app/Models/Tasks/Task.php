<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Tasks\Workspace;
use App\Models\Tasks\Task;
use App\Models\User;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Task extends Model
{
    use HasUuids;

    //TODO: Recurrance

    protected function serializeDate(\DateTimeInterface $date) : string
    {
        return $date->format('Y-m-d H:i:s');
    }

    protected $fillable = [
        'title',
        'priority',
        'order',
        'start_time',
        'end_time',
        'estimate',
        'complete',
    ];

    protected $guarded = [
        'workspace_id',
        'parent_id',
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
        "pivot",
        "workspace",
        "children",
    ];

    protected $casts = [
        'title' => 'encrypted',
        'repeat_config' => 'array',
        'repeat_mode' => 'integer',
    ];

    protected $with = [
        // "children",
    ];

    protected $appends = [
        "blocked_by",
        "blocking",
        "blocked",
        "weight",
        "time_mode",
        "has_children",
    ];

    public function workspace(): BelongsTo
    {
        return $this->belongsTo(Workspace::class);
    }

    public function parent(): BelongsTo
    {
        return $this->belongsTo(Task::class, 'parent_id');
    }

    public function children(): HasMany {
        return $this->hasMany(Task::class, 'parent_id')->orderBy('order', 'ASC');
    }

    public function dependsOn(): BelongsToMany
    {
        return $this->belongsToMany(Task::class, 'task_dependencies', 'task', 'requires');
    }

    public function requiredBy(): BelongsToMany
    {
        return $this->belongsToMany(Task::class, 'task_dependencies', 'requires', 'task');
    }

    public static function findById(string $id) {
        return Task::where("id", $id)->first();
    }

    protected function blockedBy(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->dependsOn()
                ->select(["id"])
                ->without("children")
                ->without("depends_on")
                ->without("required_by")
                ->get()->makeHidden([
                    "blocked_by",
                    "blocking",
                    "blocked",
                    "weight",
                ])
        );
    }

    protected function blocking(): Attribute
    {
        return Attribute::make(
            get: fn () => $this->requiredBy()
                ->select(["id"])
                ->without("children")
                ->without("depends_on")
                ->without("required_by")
                ->get()->makeHidden([
                    "blocked_by",
                    "blocking",
                    "blocked",
                    "weight",
                ])
        );
    }

    private function isParentTask(){
        if(count($this->children) > 0){
            return true;
        } else {
            return false;
        }
    }

    private function isChildTask(){
        if($this->parent_id){
            return true;
        } else {
            return false;
        }
    }

    protected function estimate(): Attribute
    {
        return Attribute::make(
            set: fn (?int $value) => $value,
            get: function(?int $value=null){
                if($this->isParentTask()){

                    $total = 0;
                    foreach($this->children as $task){
                        if($task->estimate && !$task->complete){
                            $total = $total + $task->estimate;
                        }
                    }

                    if($total > 0){
                        return $total;
                    } else {
                        return null;
                    }

                } else {
                    return $value;
                }
            }
        );
    }

    protected function complete(): Attribute
    {
        return Attribute::make(
            set: fn (bool $value) => $value,
            get: function(bool $value){
                if($this->isParentTask()){
                    
                    foreach($this->children as $task){
                        if(!$task->complete){
                            return false;
                        }
                    }

                    return true;

                } else {
                    return $value;
                }
            }
        );
    }

    protected function priority(): Attribute
    {
        return Attribute::make(
            set: fn (int $value) => $value,
            get: function(int $value){
                if($this->isParentTask()){
                    
                    $total = 0;
                    foreach($this->children as $task){
                        $total = $total + $task->priority;
                    }

                    return round(($total / count($this->children)));

                } else {
                    return $value;
                }
            }
        );
    }

    protected function blocked(): Attribute
    {
        return Attribute::make(
            get: function(){
                if($this->isParentTask()){
                    
                    if($this->isChildTask()){
                        foreach($this->children as $task){
                            if($task->blocked && !$task->complete){
                                return true;
                            }
                        }
                    }

                    return false;

                } else {
                    
                    foreach($this->dependsOn()->get() as $task){
                        if(!$task->complete){
                            return true;
                        }
                    }

                    return false;

                }
            }
        );
    }

    protected function timeMode(): Attribute
    {
        return Attribute::make(
            get: function(){
                if($this->getRawOriginal("start_time") != null){
                    return "start";
                } else if($this->getRawOriginal("end_time") != null){
                    return "end";
                } else {
                    return "";
                }
            }
        );
    }

    protected function hasChildren(): Attribute
    {
        return Attribute::make(
            get: function(){
                return $this->isParentTask();
            }
        );
    }

    protected function startTime(): Attribute
    {
        return Attribute::make(
            set: fn (\DateTime $value=null) => $value ? $value->format("Y-m-d H:i:s") : null,
            get: function(?string $value=null, $attributes){
                if($value){
                    return new \DateTime($value);
                } else {

                    if(!array_key_exists('end_time', $attributes) || !$attributes['end_time']){
                        return null;
                    }

                    if(!array_key_exists('estimate', $attributes) || !$attributes['estimate']){
                        return new \DateTime($attributes['end_time']);
                    }

                    $value = new \DateTime($attributes['end_time']);

                    return $value->sub(new \DateInterval("PT".$attributes['estimate']."M"));
                }
            }
        );
    }

    protected function endTime(): Attribute
    {
        return Attribute::make(
            set: fn (\DateTime $value=null) => $value ? $value->format("Y-m-d H:i:s") : null,
            get: function(?string $value=null, $attributes){
                if($value){
                    return new \DateTime($value);
                } else {

                    if(!array_key_exists('start_time', $attributes) || !$attributes['start_time']){
                        return null;
                    }

                    if(!array_key_exists('estimate', $attributes) || !$attributes['estimate']){
                        return new \DateTime($attributes['start_time']);
                    }

                    $value = new \DateTime($attributes['start_time']);

                    return $value->add(new \DateInterval("PT".$attributes['estimate']."M"));
                }
            }
        );
    }

    protected function getDueDateWeight(){
        if($this->end_time){

            $remainingMinutes = $this->workspace->getWorkingMinutesUntilDateTime($this->end_time);

            if($this->estimate){
                $timeToDoTask = $remainingMinutes / $this->estimate;

                if($timeToDoTask <= 0){
                    return 100;
                }

                if($timeToDoTask >= 1){
                    return ceil(50 / $timeToDoTask);
                }

                return 50 + ceil(50 * (1 - $timeToDoTask));

            } else {
                
                if($remainingMinutes <= 0){
                    return 100;
                }

            }
        }

        return 0;
    }

    protected function getPriorityWeight(){
        return ceil(($this->priority / 5) * 100);
    }

    protected function getBlockingWeight(){
        $required_by = $this->requiredBy()->get();
        if(count($required_by) > 0){
            $total = 0;

            foreach($required_by as $task){
                $total = $total + $task->weight;
            }

            return round(($total / count($required_by)));
        } else {
            return 0;
        }
    }

    protected function getEstimateWeight(){
        if($this->end_time || !$this->estimate){
            return 0;
        }

        if($this->estimate >= 60 || $this->estimate == 0){
            return 0;
        }

        return ceil((1.0 - ($this->estimate / 60)) * 100);
    }

    protected function weight(): Attribute
    {
        return Attribute::make(
            get: function(){
                if($this->isParentTask()){
                    
                    $total = 0;
                    foreach($this->children as $task){
                        $total = $total + $task->weight;
                    }

                    return round(($total / count($this->children)));

                } else {
                    
                    $weights = [
                        $this->getDueDateWeight(),
                        $this->getPriorityWeight(),
                        $this->getBlockingWeight(),
                        $this->getEstimateWeight(),
                    ];

                    $total = 0;
                    foreach($weights as $weight){
                        $total = $total + $weight;
                    }

                    return ceil(($total / count($weights)));

                }
            }
        );
    }

    public function setCompleted($isCompleted=true){
        $this->complete = $isCompleted;

        if($isCompleted && $this->repeat_mode != 0 && count($this->repeat_config) > 0){
            $newTask = $this->replicate();

            $newTask->complete = false;

            $listEndOrder = count(self::getSiblingTasks($this->workspace, $this->parent)) + 1;
            $nextOrder = $this->order + 1;
            $newTask->order = $nextOrder > $listEndOrder ? $nextOrder : $listEndOrder;

            if($this->timeMode == "start"){
                $date = $newTask->start_time;
            } else if($this->timeMode == "end"){
                $date = $newTask->end_time;
            } else {
                $date = new \DateTime();
                $date->setTime(0, 0, 0);
            }

            $dateIncrement = "P1D";
            if($this->repeat_mode == 3){
                $dateIncrement = "P1M";
            }

            while(true){
                $date->add(new \DateInterval($dateIncrement));

                if($this->repeat_mode == 1){
                    $needle = $date->format("N");
                } else if($this->repeat_mode == 2){
                    $needle = $date->format("j");
                } else {
                    $needle = $date->format("n");
                }

                if(in_array($needle, $this->repeat_config)){
                    break;
                }
            }

            if($this->timeMode == "start"){
                $newTask->start_time = $date;
            } else if($this->timeMode == "end"){
                $newTask->end_time = $date;
            } else {
                $newTask->end_time = $date;
            }

            $newTask->save();
        }
    }

    public static function getSiblingTasks(Workspace $workspace, ?Task $parent=null) {
        return Task::where([
            ["parent_id", "=", $parent ? $parent->id : null],
            ["workspace_id", "=", $workspace->id]
        ])->get();
    }

    public static function allByUser(User $user) {
        $orConditions = [];
        foreach(Workspace::allByUser($user) as $workspace){
            $orConditions[] = $workspace->id;
        }

        $found = Task::whereIn("workspace_id", $orConditions)->get()->toArray();

        uasort($found, function($a, $b) {
            if ($a['weight'] == $b['weight']) {
                return 0;
            }
            return ($a['weight'] < $b['weight']) ? -1 : 1;
        });

        return $found;
    }
}
