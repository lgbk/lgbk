<?php

namespace App\Models\Tasks;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use App\Models\User;
use App\Models\Tasks\Task;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Workspace extends Model
{
    use HasUuids;

    protected $fillable = [
        'title',
        'color',
        'order'
    ];

    protected $guarded = [
        'user_id'
    ];

    protected $hidden = [
        'updated_at',
        'created_at',
        'user_id',
        'user',
    ];

    protected $casts = [
        'title' => 'encrypted',
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function tasks(): HasMany {
        return $this->hasMany(Task::class);
    }

    protected function color(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => "#".$value,
            set: fn (string $value) => str_replace("#","", $value)
        );
    }

    public function getWorkingMinutesUntilDateTime(\DateTime $until){
        $now = new \DateTime();
        $diffMinutes = ceil(($until->format("U") - $now->format("U")) / 60);
        return $diffMinutes;
    }

    public static function allByUser(User $user) {
        return Workspace::where("user_id", $user->id)->orderBy('order', 'ASC')->get();
    }

    public static function findById(string $id) {
        return Workspace::where("id", $id)->first();
    }

    public function getTopLevelTasks() {
        return $this->tasks()->where('parent_id', null)->orderBy("order", "ASC")->get();
    }

    public function getAllTasks() {
        return $this->tasks()->orderBy("order", "ASC")->get();
    }
}
