<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use App\Models\User;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

class AuthToken extends Model
{
    use HasUuids;

    const AUTH_TYPE = 'auth';
    const REFRESH_TYPE = 'refresh';

    protected function serializeDate(\DateTimeInterface $date) : string
    {
        return $date->format('Y-m-d H:i:s');
    }

    protected $fillable = [
        'token',
        'type',
        'used',
    ];

    protected $guarded = [
        'expires',
        'user_id',
    ];

    protected $hidden = [
        'id',
        'used',
        'user_id',
        'user',
        'token_hash',
        'updated_at',
        'created_at',
        'sibling_id',
    ];

    protected $casts = [
        'expires' => 'datetime'
    ];

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function sibling(): HasOne
    {
        return $this->hasOne(AuthToken::class, 'sibling_id');
    }

    protected function token(): Attribute
    {
        return Attribute::make(
            get: fn (string $value) => Crypt::decryptString($value),
            set: fn (string $value) => [
                'token' => Crypt::encryptString($value),
                'token_hash' => hash('sha256', $value),
            ],
        );
    }

    public function isValid() {
        $now = new \DateTime();

        if($this->expires <= $now){
            return false;
        }

        if($this->used){
            return false;
        }

        return true;
    }

    public static function findRefreshToken(string $token) {
        return AuthToken::where([
            ["token_hash", "=", hash('sha256', $token)],
            ["type", "=", self::REFRESH_TYPE]
        ])->first();
    }

    public static function findAuthToken(string $token) {
        return AuthToken::where([
            ["token_hash", "=", hash('sha256', $token)],
            ["type", "=", self::AUTH_TYPE]
        ])->first();
    }

    private function calculateExpirationDateTime() {
        if($this->type == self::REFRESH_TYPE){
            $interval = 'P7D';
        } else {
            $interval = 'PT2H';
        }

        $expires = new \DateTime();
        $expires->add(new \DateInterval($interval));
        return $expires;
    }

    private static function issue(User $user, string $type){
        $token = new AuthToken();

        $token->user_id = $user->id;

        $token->type = $type;

        $token->used = false;
        $token->expires = $token->calculateExpirationDateTime();

        $token->token = bin2hex(random_bytes(32));

        $token->save();
        return $token;
    }

    public static function issuePair(User $user){
        $auth = self::issue($user, self::AUTH_TYPE);
        $refresh = self::issue($user, self::REFRESH_TYPE);

        $auth->sibling_id = $refresh->id;
        $refresh->sibling_id = $auth->id;

        $auth->save();
        $refresh->save();

        return [
            "auth" => $auth,
            "refresh" => $refresh
        ];
    }

    // public function __construct(User $user, string $type){
    //     parent::__construct();

    //     $this->user_id = $user->id;

    //     $this->type = $type;

    //     $this->used = false;
    //     $this->expires = $this->calculateExpirationDateTime();

    //     $this->token = bin2hex(random_bytes(32));
    // }
}
