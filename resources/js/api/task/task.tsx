import {
    authenticatedPostRequest,
    authenticatedGetRequest,
    authenticatedPatchRequest,
    authenticatedPutRequest,
    authenticatedDeleteRequest,
    dateToDateTimeString,
} from "../api";
import { Workspace } from "../workspace/workspace"

export type RelatedTask = {
    id: string
}

export enum TaskPriority {
    VeryLow = 1,
    Low = 2,
    Normal = 3,
    High = 4,
    VeryHigh = 5
}

export enum TaskRepeatMode {
    Off = 0,
    Weekdays = 1,
    MonthDays = 2,
    YearMonths = 3
}

export enum TaskTimeMode {
    StartAt = "start",
    DueBy = "end",
    None = ""
}

export type Task = {
    id: string;
    workspace_id: string,
    title: string;
    order: number;
    priority: TaskPriority;
    start_time?: Date;
    end_time?: Date;
    estimate?: number;
    complete: boolean;
    parent_id?: string;
    blocked_by: RelatedTask[],
    blocking: RelatedTask[],
    blocked: boolean,
    weight: number,
    time_mode: TaskTimeMode,
    repeat_mode: TaskRepeatMode,
    repeat_always: boolean,
    repeat_config: number[]
    has_children: boolean,
    project_id?: string
};


export const createTask = async (
    workspace: Workspace,
    title: string,
    priority: TaskPriority = 3,
    parent?: string,
    startTime?: Date,
    endTime?: Date,
    estimate?: number
): Promise<Task> => {
    const data: any = { 
        workspace: workspace.id,
        title: title,
        priority: priority
    };

    if (parent) {
        data["parent"] = parent;
    }

    if (startTime) {
        data["start_time"] = dateToDateTimeString(startTime);
    }

    if (endTime) {
        data["end_time"] = dateToDateTimeString(endTime);
    }

    if (estimate) {
        data["estimate"] = estimate;
    }

    const response = await authenticatedPostRequest("/api/task", data);

    return response.json();
};

export const loadTasks = async (): Promise<Task[]> => {
    const response = await authenticatedGetRequest("/api/task");
    return response.json();
};

export const updateTask = async (
    task: Task,
    attributes: {
        title?: string,
        priority?: TaskPriority,
        start_time?: Date,
        end_time?: Date,
        estimate?: number,
        complete?: boolean,
        order?: number,
        workspace?: Workspace,
        parent?: Task | undefined,
        repeat_mode?: TaskRepeatMode,
        repeat_always?: boolean,
        repeat_config?: number[],
    }
): Promise<Task[]> => {
    const data: any = {}

    if(attributes.title){
        data['title'] = attributes.title;
    }

    if(attributes.priority){
        data['priority'] = attributes.priority;
    }

    if(attributes.hasOwnProperty("start_time")){
        if(attributes.start_time){
            data['start_time'] = dateToDateTimeString(attributes.start_time);
        } else {
            data['start_time'] = null;
        }
    }

    if(attributes.hasOwnProperty("end_time")){
        if(attributes.end_time){
            data['end_time'] = dateToDateTimeString(attributes.end_time);
        } else {
            data['end_time'] = null;
        }
    }

    if(attributes.hasOwnProperty("estimate")){
        data['estimate'] = attributes.estimate;
    }

    if(attributes.hasOwnProperty("complete")){
        data['complete'] = attributes.complete;
    }

    if(attributes.order){
        data['order'] = attributes.order;
    }

    if(attributes.workspace){
        data['workspace'] = attributes.workspace.id;
    }

    if(attributes.hasOwnProperty("parent")){
        if(attributes.parent){
            data['parent'] = attributes.parent.id;
        } else {
            data['parent'] = null;
        }
    }

    if(attributes.hasOwnProperty("repeat_mode")){
        data['repeat_mode'] = attributes.repeat_mode;
    }

    if(attributes.hasOwnProperty("repeat_always")){
        data['repeat_always'] = attributes.repeat_always;
    }

    if(attributes.repeat_config){
        data['repeat_config'] = attributes.repeat_config;
    }

    const response = await authenticatedPatchRequest("/api/task/"+String(task.id), data);

    return response.json();
};


export const updateTaskDependencies = async (
    task: Task,
    dependencies: Task[]
): Promise<Task[]> => {
    const data: any = {
        "dependencies": []
    }

    for(let i in dependencies){
        data['dependencies'].push(dependencies[i].id);
    }

    const response = await authenticatedPutRequest("/api/task/"+String(task.id)+"/dependency", data);

    return response.json();
};


export const deleteTask = async (
    task: Task
): Promise<Task[]> => {
    const response = await authenticatedDeleteRequest("/api/task/"+String(task.id));

    return response.json();
};
