import { basicPostRequest, authenticatedGetRequest } from "../api"

type User = {
    "id": string,
    "first_name": string,
    "last_name": string,
} | undefined


export const registerAccount = async (email: string, password: string, firstName: string, lastName: string): Promise<User> => {
    const response = await basicPostRequest("/api/user/register", {
        "email": email,
        "password": password,
        "first_name": firstName,
        "last_name": lastName,
    })

    if(response.ok){
        return response.json();
    } else {
        return undefined;
    }
}


export const getCurrentUser = async (): Promise<User> => {
    const response = await authenticatedGetRequest("/api/user")

    if(response.ok){
        return response.json();
    } else {
        return undefined;
    }
}