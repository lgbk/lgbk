import {
    authenticatedPostRequest,
    authenticatedGetRequest,
    authenticatedPatchRequest,
    authenticatedDeleteRequest,
} from "../api";

export type Workspace = {
    id: string;
    order: number;
    title: string;
    color: string;
};

export const createWorkspace = async (
    title: string,
    color?: string
): Promise<Workspace> => {
    const data: any = { title: title };

    if (color) {
        data["color"] = color;
    }

    const response = await authenticatedPostRequest("/api/workspace", data);

    return response.json();
};

export const loadWorkspaces = async (): Promise<Workspace[]> => {
    const response = await authenticatedGetRequest("/api/workspace");
    return response.json();
};

export const updateWorkspace = async (
    id: string,
    title: string,
    color?: string
): Promise<Workspace> => {
    const data: any = { title: title };

    if (color) {
        data["color"] = color;
    }

    const response = await authenticatedPatchRequest(
        "/api/workspace/" + id,
        data
    );

    return response.json();
};

export const deleteWorkspace = async (id: string): Promise<Workspace> => {
    const response = await authenticatedDeleteRequest("/api/workspace/" + id);
    return response.json();
};
