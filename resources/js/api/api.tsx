import { retrieveAuthToken } from "./auth/auth"

async function mergeHeaders(headers: any={}, includeAuth: boolean=false) {
    headers["Content-Type"] = "application/json";

    if(includeAuth){
        headers["Authorization"] = "Bearer " + await retrieveAuthToken();
    }

    return headers;
}

async function request(method: string, url: string, authorized: boolean=false, body: any={}, params: any={}, headers: any={}){
    const request: any = {
        "method": method,
        "mode": "cors",
        "cache": "no-cache",
        "headers": await mergeHeaders(headers, authorized),
        "redirect": "follow",
        "referrerPolicy": "no-referrer"
    }

    if(method.toLowerCase() != "get"){
        request['body'] = JSON.stringify(body);
    }

    const response = await fetch(url, request);
    return response;
}


export async function basicGetRequest(url: string){
    return await request("GET", url, false);
}

export async function basicPostRequest(url: string, body: any={}){
    return await request("POST", url, false, body);
}


export async function authenticatedGetRequest(url: string){
    return await request("GET", url, true);
}

export async function authenticatedPostRequest(url: string, body: any={}){
    return await request("POST", url, true, body);
}

export async function authenticatedPutRequest(url: string, body: any={}){
    return await request("PUT", url, true, body);
}

export async function authenticatedPatchRequest(url: string, body: any={}){
    return await request("PATCH", url, true, body);
}

export async function authenticatedDeleteRequest(url: string){
    return await request("DELETE", url, true);
}


export function dateToDateTimeString(d: Date | undefined): string | undefined {
    if(!d){
        return;
    }

    const ensureLeadingZero = (val: number): string => {
        if(val <= 9){
            return "0" + String(val);
        } else {
            return String(val);
        }
    }

    let formatted: string = String(d.getFullYear());
    formatted = formatted+"-"+String(ensureLeadingZero(d.getMonth() + 1));
    formatted = formatted+"-"+String(ensureLeadingZero(d.getDate()));

    formatted = formatted+" "+ensureLeadingZero(d.getHours());
    formatted = formatted+":"+ensureLeadingZero(d.getMinutes());
    formatted = formatted+":"+ensureLeadingZero(d.getSeconds());

    return formatted;
}

export function dateTimeStringToDate(dt: string): Date {
    const date = new Date(dt);

    return date;
}