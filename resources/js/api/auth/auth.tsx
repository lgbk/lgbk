import { basicPostRequest, authenticatedPostRequest } from "../api"
import { store } from "../../redux/store"
import { setIsLoggedIn } from "../../redux/auth/slice"


export function localTokensAreValid(): boolean {
    let auth = window.localStorage.getItem("authToken");
    if(auth){
        
        let authExpires = window.localStorage.getItem("authExpires");
        if(authExpires){
            let now = new Date();
            let authExpiresDate = new Date(authExpires)

            if(now >= authExpiresDate){

                let refresh = window.localStorage.getItem("refreshToken");
                if(refresh){
                    let refreshExpires = window.localStorage.getItem("refreshExpires");
                    if(refreshExpires){
                        let refreshExpiresDate = new Date(refreshExpires)
                        if(now < refreshExpiresDate){
                            return true;
                        }
                    }
                }

            } else {
                return true;
            }
        }

    }

    return false;
}


export async function retrieveAuthToken(): Promise<string | null> {
    let auth = window.localStorage.getItem("authToken");
    if(auth){
        
        let authExpires = window.localStorage.getItem("authExpires");
        if(authExpires){
            let now = new Date();
            let authExpiresDate = new Date(authExpires)

            if(now >= authExpiresDate){

                let refresh = window.localStorage.getItem("refreshToken");
                if(refresh){
                    let refreshExpires = window.localStorage.getItem("refreshExpires");
                    if(refreshExpires){
                        let refreshExpiresDate = new Date(refreshExpires)
                        if(now < refreshExpiresDate){

                            let refresh_success = await loginWithRefreshToken(refresh);
                            if(refresh_success){
                                return window.localStorage.getItem("authToken");
                            }

                        }
                    }
                }

            } else {
                return auth;
            }
        }

    }

    return null;
}

export const loginWithEmailAndPassword = async (email: string, password: string): Promise<boolean> => {
    const response = await basicPostRequest("/api/auth/login", {
        "email": email,
        "password": password
    })

    if(response.ok){
        const payload = await response.json();

        window.localStorage.setItem("authToken", payload.auth.token);
        window.localStorage.setItem("authExpires", payload.auth.expires);

        window.localStorage.setItem("refreshToken", payload.refresh.token);
        window.localStorage.setItem("refreshExpires", payload.refresh.expires);

        store.dispatch(setIsLoggedIn({isLoggedIn: true}));

        return true;
    } else {
        return false;
    }
}

export const loginWithRefreshToken = async (token: string): Promise<boolean> => {
    const response = await basicPostRequest("/api/auth/refresh", {
        "token": token
    })

    if(response.ok){
        const payload = await response.json();

        window.localStorage.setItem("authToken", payload.auth.token);
        window.localStorage.setItem("authExpires", payload.auth.expires);

        window.localStorage.setItem("refreshToken", payload.refresh.token);
        window.localStorage.setItem("refreshExpires", payload.refresh.expires);

        store.dispatch(setIsLoggedIn({isLoggedIn: true}));

        return true;
    } else {
        return false;
    }
}

export const logout = async (): Promise<any> => {
    const response = await authenticatedPostRequest("/api/auth/logout")

    window.localStorage.removeItem("authToken");
    window.localStorage.removeItem("authExpires");

    window.localStorage.removeItem("refreshToken");
    window.localStorage.removeItem("refreshExpires");

    store.dispatch(setIsLoggedIn({isLoggedIn: false}));

    if(response.ok){
        return true
    } else {
        return false;
    }
}