import { createSlice, PayloadAction } from "@reduxjs/toolkit"
import { RootState } from "../store"

export interface StructureState {
    menuOpen: boolean
}

const initialState: StructureState = {
    menuOpen: true
}

export const structureSlice = createSlice({
  name: "structure",
  initialState,
  reducers: {
    setMenuIsOpen: (state, action: PayloadAction<{isOpen: boolean}>) => {
      state.menuOpen = action.payload.isOpen;
    },
  }
})

export const { setMenuIsOpen } = structureSlice.actions

export const selectMenuIsOpen = (state: RootState) => state.structure.menuOpen

export default structureSlice.reducer