import { selectMenuIsOpen } from "./slice"
import { useSelector } from "react-redux"

export const useMenuIsOpen = (): boolean => {
    const isOpen = useSelector(selectMenuIsOpen);
    return isOpen
}