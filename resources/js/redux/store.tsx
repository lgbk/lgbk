import { configureStore, ThunkAction, Action } from "@reduxjs/toolkit"
import { authSlice } from "./auth/slice"
import { structureSlice } from "./structure/slice"
import { workspaceSlice } from "./workspace/slice"
import { taskSlice } from "./task/slice"
import thunkMiddleware from 'redux-thunk';

export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    structure: structureSlice.reducer,
    workspace: workspaceSlice.reducer,
    task: taskSlice.reducer,
  },
  middleware: [
    thunkMiddleware
  ]
})

export type AppDispatch = typeof store.dispatch
export type RootState = ReturnType<typeof store.getState>
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>