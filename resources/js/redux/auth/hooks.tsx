import { selectAuthLoggedIn } from "./slice"
import { useSelector, useDispatch } from "react-redux"
import { useNavigate } from "react-router-dom"
import { localTokensAreValid } from "../../api/auth/auth"
import { setIsLoggedIn } from "./slice"

export const useLoggedIn = (): boolean => {
    const isLoggedIn = useSelector(selectAuthLoggedIn);
    const navigate = useNavigate();
    const dispatch = useDispatch();

    if(!isLoggedIn){
        if(localTokensAreValid()){
            dispatch(setIsLoggedIn({isLoggedIn: true}));
            return true;
        }
    }

    if(!isLoggedIn){
        navigate("/");
    }

    return isLoggedIn;
}