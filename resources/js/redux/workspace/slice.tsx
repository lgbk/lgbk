import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import { RootState, AppThunk } from "../store"
import { 
  createWorkspace as createWorkspaceApi,
  updateWorkspace as updateWorkspaceApi,
  loadWorkspaces as loadWorkspacesApi,
  deleteWorkspace as deleteWorkspaceApi,
  Workspace
} from "../../api/workspace/workspace"

export interface WorkspaceState {
    workspaces: Workspace[]
    loading: boolean
    isLoaded: boolean
}

const initialState: WorkspaceState = {
    workspaces: [],
    loading: false,
    isLoaded: false,
}

export const loadWorkspaces = createAsyncThunk(
  "workspace/loadWorkspaces",
  async () => {
    const response = await loadWorkspacesApi();
    return response;
  },
)

export const createWorkspace = createAsyncThunk(
  "workspace/createWorkspace",
  async (data: {title: string, color?: string}) => {
    const response = await createWorkspaceApi(data.title, data.color)
    return response;
  },
)

export const updateWorkspace = createAsyncThunk(
  "workspace/updateWorkspace",
  async (data: {id: string, title: string, color?: string}) => {
    const response = await updateWorkspaceApi(data.id, data.title, data.color)
    return response;
  },
)

export const deleteWorkspace = createAsyncThunk(
  "workspace/deleteWorkspace",
  async (data: {id: string}) => {
    const response = await deleteWorkspaceApi(data.id)
    return response;
  },
)

export const workspaceSlice = createSlice({
  name: "workspace",
  initialState,
  reducers: {
    setIsLoaded: (state, action: PayloadAction<{isLoaded: boolean}>) => {
      state.isLoaded = action.payload.isLoaded;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(loadWorkspaces.pending, (state) => {
        state.loading = true;
      })
      .addCase(loadWorkspaces.fulfilled, (state, action) => {
        state.loading = false;
        state.workspaces = action.payload;
        state.isLoaded = true;
      })
      .addCase(loadWorkspaces.rejected, (state) => {
        state.loading = false;
      })

      .addCase(createWorkspace.pending, (state) => {
        state.loading = true;
      })
      .addCase(createWorkspace.fulfilled, (state, action) => {
        state.loading = false;
        state.workspaces.push(action.payload);
      })
      .addCase(createWorkspace.rejected, (state) => {
        state.loading = false;
      })

      .addCase(updateWorkspace.pending, (state) => {
        state.loading = true;
      })
      .addCase(updateWorkspace.fulfilled, (state, action) => {
        state.loading = false;

        const newSpaces: Workspace[] = [];

        state.workspaces.forEach((workspace) => {
          if(workspace.id == action.payload.id){
            newSpaces.push(action.payload);
          } else {
            newSpaces.push(workspace);
          }
        })

        state.workspaces = newSpaces;
      })
      .addCase(updateWorkspace.rejected, (state) => {
        state.loading = false;
      })

      .addCase(deleteWorkspace.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteWorkspace.fulfilled, (state, action) => {
        state.loading = false;

        const newSpaces: Workspace[] = [];

        state.workspaces.forEach((workspace) => {
          if(workspace.id != action.payload.id){
            newSpaces.push(workspace);
          }
        })

        state.workspaces = newSpaces;
      })
      .addCase(deleteWorkspace.rejected, (state) => {
        state.loading = false;
      })
  },
})

export const { setIsLoaded } = workspaceSlice.actions

export const selectWorkspaces = (state: RootState) => state.workspace.workspaces
export const selectWorkspacesLoading = (state: RootState) => state.workspace.loading
export const selectWorkspacesAreLoaded = (state: RootState) => state.workspace.isLoaded

export default workspaceSlice.reducer