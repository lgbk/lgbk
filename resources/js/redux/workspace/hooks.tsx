import { useEffect } from "react";
import { Workspace } from "../../api/workspace/workspace"
import { selectWorkspaces, loadWorkspaces, selectWorkspacesAreLoaded, selectWorkspacesLoading } from "./slice"
import { useSelector, useDispatch } from "react-redux"

var workspaceHookLoading: boolean = false;


export const useWorkspaces = (): Workspace[] => {
    const dispatch = useDispatch();
    const workspaces = useSelector(selectWorkspaces);
    const isLoaded = useSelector(selectWorkspacesAreLoaded);
    const loading = useSelector(selectWorkspacesLoading);

    useEffect(() => {
        if(!isLoaded && !workspaceHookLoading){
            workspaceHookLoading = true;
            dispatch(loadWorkspaces());
        } else if(isLoaded){
            workspaceHookLoading = false;
        }
    }, [isLoaded, loading])

    return workspaces
}


export const useWorkspaceById = (id: string|undefined): Workspace | undefined => {
    const workspaces = useWorkspaces();

    if(workspaces.length == 0){
        return;
    }

    if(!id){
        return;
    }

    const workspace = workspaces.find((workspace) => {
        if(workspace.id == id){
            return true;
        }
    })

    return workspace;
}