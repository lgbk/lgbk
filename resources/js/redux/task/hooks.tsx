import { useEffect } from "react";
import { Workspace } from "../../api/workspace/workspace"
import { Task, TaskPriority, TaskRepeatMode } from "../../api/task/task"
import { selectTasks, loadTasks, selectTasksAreDirty, updateTask, updateTaskDependencies, deleteTask, selectTasksLoading } from "./slice"
import { useSelector, useDispatch } from "react-redux"

var taskHookLoading: boolean = false;


export const useTasks = (includeCompleted: boolean = true): Task[] => {
    const dispatch = useDispatch();
    const tasks = useSelector(selectTasks);
    const isDirty = useSelector(selectTasksAreDirty);
    const loading = useSelector(selectTasksLoading);


    useEffect(() => {
        if(isDirty && !taskHookLoading){
            taskHookLoading = true;
            dispatch(loadTasks());
        } else if(!isDirty){
            taskHookLoading = false;
        }
    }, [isDirty])

    if(includeCompleted){
        return tasks;
    } else {
        return tasks.filter((task) => task.has_children || !task.complete);
    }
}


export const useTaskFinder = (search: string): Task[] => {
    const tasks = useTasks();

    if(!search){
        return [];
    }

    return tasks.filter((task: Task) => {
        return task.title.toLowerCase().includes(search.toLowerCase())
    })
}


export const useWorkspaceTasks = (workspace: Workspace|undefined): Task[] => {
    const tasks = useTasks();

    if(!workspace){
        return [];
    }

    return tasks.filter((task: Task) => {
        return task.workspace_id == workspace.id && !task.parent_id
    })
}


export const useTaskById = (id: string|undefined): Task|undefined => {
    const tasks = useTasks();

    if(!id){
        return;
    }

    for(let i in tasks){
        if(tasks[i].id == id){
            return tasks[i];
        }
    }
}


export const useTasksById = (ids: string[]): Task[] => {
    const tasks = useTasks();

    return tasks.filter((task) => {
        return ids.includes(task.id);
    })
}


export const useParentTaskOf = (task: Task|undefined): Task|undefined => {
    const parent = useTaskById(task?.parent_id);
    return parent
}


export const useTaskLineage = (task: Task|undefined, includeProject: boolean = true): Task[] => {
    const tasks = useTasks();
    const found: Task[] = [];

    if(task){
        let current = task;
        while(current.parent_id){
            current = tasks.filter((t) => t.id == current.parent_id)[0];
            found.push(current);
        }

        if(!includeProject){
            found.pop();
        }

        found.reverse();
    }

    return found;
}


type ProjectFinder = (t: Task|undefined) => Task|undefined

export const useTaskProjectFinder = (): ProjectFinder => {
    const tasks = useTasks();

    return (task: Task|undefined): Task|undefined => {
        let found: Task | undefined;

        if(!task){
            return;
        }

        const findParent = (task: Task) => {
            return tasks.filter((t) => t.id == task.parent_id)[0];
        }

        let check = task;
        while(!found){
            if(!check.parent_id){
                found = check;
                break;
            } else {
                check = findParent(check);
            }
        }

        if(found.id == task.id){
            return;
        }

        return found
    }
}


export const useTaskProject = (task: Task|undefined): Task|undefined => {
    const projectFinder = useTaskProjectFinder();
    return projectFinder(task);
}


export const useChildTasksOf = (task: Task|undefined, recursive: boolean = false, includeParents: boolean = true, includeCompleted: boolean = true): Task[] => {
    const tasks = useTasks(includeCompleted);
    const children: Task[] = [];

    const findRecursiveChildrenOf = (id: string): Task[] => {
        const found: Task[] = [];

        for(let i in tasks){
            if(tasks[i].parent_id == id){

                let grandChildren = findRecursiveChildrenOf(tasks[i].id);

                if(includeParents || grandChildren.length == 0){
                    found.push(tasks[i]);
                }

                for(let c in grandChildren){
                    found.push(grandChildren[c]);
                }

            }
        }

        return found;
    }

    if(!task){
        return [];
    }

    for(let i in tasks){
        if(tasks[i].parent_id == task.id){
            if(recursive){

                let grandChildren = findRecursiveChildrenOf(tasks[i].id);

                if(includeParents || grandChildren.length == 0){
                    children.push(tasks[i]);
                }

                for(let c in grandChildren){
                    children.push(grandChildren[c]);
                }

            } else {
                children.push(tasks[i]);
            }
        }
    }

    return children;
}


export const useTaskIsParent = (task: Task): boolean => {
    const children = useChildTasksOf(task);

    if(!task.parent_id || children.length > 0){
        return true;
    } else {
        return false;
    }
}


export const useTaskEstimateString = (task: Task|undefined): string|undefined => {
    if(!task || !task.estimate){
        return;
    }

    let eta = "";
    let remaining = task.estimate;

    if(remaining >= 480){
        const days = Math.floor(remaining / 480);
        eta = eta+String(days)+" day(s)"
        remaining = remaining - (days * 480);
    }

    if(remaining >= 60){
        const hours = Math.floor(remaining / 60);
        eta = eta+" "+String(hours)+" hour(s)"
        remaining = remaining - (hours * 60);
    }

    if(remaining > 0){
        const mins = Math.floor(remaining);
        eta = eta+" "+String(mins)+" min(s)"
    }

    eta = eta+" ("+String((task.estimate / 60).toFixed(2))+" hours)";
    
    return eta;
}


export const useTaskUpdater = (
    task: Task
): Function => {
    const dispatch = useDispatch();
    
    const doUpdate = async (
        attributes: {
            title?: string,
            priority?: TaskPriority,
            start_time?: Date,
            end_time?: Date,
            estimate?: number,
            complete?: boolean,
            order?: number,
            workspace?: Workspace,
            parent?: Task | undefined,
            repeat_mode?: TaskRepeatMode,
            repeat_always?: boolean,
            repeat_config?: number[],
        }
    ) => {
        let result = await dispatch(updateTask({task: task, attributes: attributes}));
        return result;
    }

    return doUpdate;
}

export const useUpdateTaskDependencies = (
    task: Task
): Function => {
    const dispatch = useDispatch();
    
    const doUpdate = async (
        dependencies: Task[]
    ) => {
        let result = await dispatch(updateTaskDependencies({task: task, dependencies: dependencies}));
        return result;
    }

    return doUpdate;
}

export const useDeleteTask = (): Function => {
    const dispatch = useDispatch();
    
    const doUpdate = async (
        task: Task
    ) => {
        let result = await dispatch(deleteTask({task: task}));
        return result;
    }

    return doUpdate;
}

export const useTodaysTasks = (includeCompleted: boolean = false): Task[] => {
    const tasks = useTasks(includeCompleted);

    const today = (new Date());
    today.setHours(23,59,59,999);

    const filtered = tasks.filter((task) => {

        if(!task.parent_id || task.has_children){
            return false;
        }

        if(task.start_time){
            if(task.start_time > today){
                return false;
            }
        } else {
            return false;
        }

        if(task.complete){
            return false;
        }

        return true;
    })

    const sorted = filtered.sort((a, b) => {
        if(a.weight == b.weight){
            return 0;
        }

        if(a.weight > b.weight){
            return -1;
        } else {
            return 1;
        }
    });

    return sorted;
}