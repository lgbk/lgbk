import { createAsyncThunk, createSlice, PayloadAction } from "@reduxjs/toolkit"
import { RootState, AppThunk } from "../store"
import { 
  createTask as createTaskApi,
  loadTasks as loadTasksApi,
  updateTask as updateTaskApi,
  updateTaskDependencies as updateTaskDependenciesApi,
  deleteTask as deleteTaskApi,
  TaskPriority,
  Task,
  TaskRepeatMode
} from "../../api/task/task"
import { Workspace } from "../../api/workspace/workspace"
import { dateTimeStringToDate } from "../../api/api"

export interface WorkspaceState {
    tasks: Task[]
    loading: boolean
    isDirty: boolean
}

const initialState: WorkspaceState = {
    tasks: [],
    loading: false,
    isDirty: true,
}

export const loadTasks = createAsyncThunk(
  "task/loadTasks",
  async () => {
    const response = await loadTasksApi();
    return response;
  },
)

export const createTask = createAsyncThunk(
  "task/createTask",
  async (data: {
    workspace: Workspace,
    title: string,
    priority?: TaskPriority,
    parent?: string,
    startTime?: Date,
    endTime?: Date,
    estimate?: number
  }) => {
    const response = await createTaskApi(
      data.workspace, 
      data.title,
      data.priority,
      data.parent,
      data.startTime,
      data.endTime,
      data.estimate,
    )
    return response;
  },
)

export const updateTask = createAsyncThunk(
  "task/updateTask",
  async (data: {
    task: Task,
    attributes: {
        title?: string,
        priority?: TaskPriority,
        start_time?: Date,
        end_time?: Date,
        estimate?: number,
        complete?: boolean,
        order?: number,
        workspace?: Workspace,
        parent?: Task | undefined,
        repeat_mode?: TaskRepeatMode,
        repeat_always?: boolean,
        repeat_config?: number[],
    }
  }) => {
    const response = await updateTaskApi(data.task, data.attributes)
    return response;
  },
)

export const updateTaskDependencies = createAsyncThunk(
  "task/updateTaskDependencies",
  async (data: {
    task: Task,
    dependencies: Task[]
  }) => {
    const response = await updateTaskDependenciesApi(data.task, data.dependencies)
    return response;
  },
)

export const deleteTask = createAsyncThunk(
  "task/deleteTask",
  async (data: {
    task: Task
  }) => {
    const response = await deleteTaskApi(data.task)
    return response;
  },
)

// export const updateWorkspace = createAsyncThunk(
//   "workspace/updateWorkspace",
//   async (data: {id: string, title: string, color?: string}) => {
//     const response = await updateWorkspaceApi(data.id, data.title, data.color)
//     return response;
//   },
// )

// export const deleteWorkspace = createAsyncThunk(
//   "workspace/deleteWorkspace",
//   async (data: {id: string}) => {
//     const response = await deleteWorkspaceApi(data.id)
//     return response;
//   },
// )

const castIncomingTask = (task: Task) => {
  if(task.start_time){
    //@ts-ignore
    task.start_time = dateTimeStringToDate(task.start_time);
  }

  if(task.end_time){
    //@ts-ignore
    task.end_time = dateTimeStringToDate(task.end_time);
  }

  return task;
}

const castIncomingTasks = (tasks: Task[]) => {
  const casted: Task[] = [];

  for(let ti in tasks){
    let task = castIncomingTask(tasks[ti]);
    casted.push(task);
  }

  return casted
}


export const taskSlice = createSlice({
  name: "task",
  initialState,
  reducers: {
    setIsDirty: (state, action: PayloadAction<{isDirty: boolean}>) => {
      state.isDirty = action.payload.isDirty;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(createTask.pending, (state) => {
        state.loading = true;
      })
      .addCase(createTask.fulfilled, (state, action) => {
        state.loading = false;
        state.isDirty = true;
      })
      .addCase(createTask.rejected, (state) => {
        state.loading = false;
      })

      .addCase(loadTasks.pending, (state) => {
        state.loading = true;
      })
      .addCase(loadTasks.fulfilled, (state, action) => {
        state.loading = false;
        state.isDirty = false;
        state.tasks = castIncomingTasks(action.payload);
      })
      .addCase(loadTasks.rejected, (state) => {
        state.loading = false;
      })

      .addCase(updateTask.pending, (state) => {
        state.loading = true;
      })
      .addCase(updateTask.fulfilled, (state, action) => {
        state.loading = false;
        state.isDirty = false;
        state.tasks = castIncomingTasks(action.payload);
      })
      .addCase(updateTask.rejected, (state) => {
        state.loading = false;
      })

      .addCase(updateTaskDependencies.pending, (state) => {
        state.loading = true;
      })
      .addCase(updateTaskDependencies.fulfilled, (state, action) => {
        state.loading = false;
        state.isDirty = false;
        state.tasks = castIncomingTasks(action.payload);
      })
      .addCase(updateTaskDependencies.rejected, (state) => {
        state.loading = false;
      })

      .addCase(deleteTask.pending, (state) => {
        state.loading = true;
      })
      .addCase(deleteTask.fulfilled, (state, action) => {
        state.loading = false;
        state.isDirty = false;
        state.tasks = castIncomingTasks(action.payload);
      })
      .addCase(deleteTask.rejected, (state) => {
        state.loading = false;
      })
  },
})

export const { setIsDirty } = taskSlice.actions

export const selectTasks = (state: RootState) => state.task.tasks
export const selectTasksLoading = (state: RootState) => state.task.loading
export const selectTasksAreDirty = (state: RootState) => state.task.isDirty

export default taskSlice.reducer