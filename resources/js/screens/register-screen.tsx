import React, { useState } from "react"
import { InputText, InputPassword } from "../components/forms/input"
import { PrimaryButton } from "../components/forms/button"
import { registerAccount } from "../api/user/user"
import { loginWithEmailAndPassword } from "../api/auth/auth"

export const RegisterScreen: React.FC = () => {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");

    const [email, setEmail] = useState("");

    const [passwordOne, setPasswordOne] = useState("");
    const [passwordTwo, setPasswordTwo] = useState("");

    const [loading, setLoading] = useState(false);

    const doRegistration = async function(){
        if(passwordOne == passwordTwo){
            if(passwordOne && email && firstName && lastName){
                var registered = await registerAccount(email, passwordOne, firstName, lastName);
                
                const success = await loginWithEmailAndPassword(email, passwordOne);
                setLoading(false);

                setPasswordOne("");
                setPasswordTwo("");
            }
        }
    }

    return (
        <div className="w-full flex justify-center">
            <div className="shrink-0 w-96">
                
                <h1 className="mb-10">Register</h1>

                <div>

                    <InputText className="mb-3" label="First name" value={firstName} onUpdateValue={setFirstName} disabled={loading} />
                    <InputText className="mb-10" label="Last name" value={lastName} onUpdateValue={setLastName} disabled={loading} />

                    <InputText className="mb-10" label="Email address" value={email} onUpdateValue={setEmail} disabled={loading} />

                    <InputPassword className="mb-3" label="Enter password" value={passwordOne} onUpdateValue={setPasswordOne} disabled={loading} />
                    <InputPassword className="mb-10" label="Reepeat password" value={passwordTwo} onUpdateValue={setPasswordTwo} disabled={loading} />

                    <PrimaryButton
                        disabled={loading}
                        onClick={doRegistration}
                    >
                        Create account
                    </PrimaryButton>

                </div>

            </div>
        </div>
    )
}