import React, { useState, useEffect } from "react"
import { InputText, InputPassword } from "../components/forms/input"
import { PrimaryButton } from "../components/forms/button"
import { Link } from "react-router-dom"
import { loginWithEmailAndPassword } from "../api/auth/auth"
import { useNavigate } from "react-router-dom"

export const LoginScreen: React.FC = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [error, setError] = useState(false);
    const [loading, setLoading] = useState(false);

    const navigate = useNavigate();

    const doLogin = async function(){
        setError(false);
        setLoading(true);

        const success = await loginWithEmailAndPassword(email, password);
        setLoading(false);
        
        if(!success){
            setError(true);
        } else {
            navigate("/today");
        }
    }

    return (
        <div className="min-h-full w-full flex justify-center items-center">
            <div className="shrink-0 w-96">
                
                <h1 className="mb-5">Login</h1>

                {error ? (
                    <div className="mb-5 text-red-500 font-bold text-center">
                        Login details incorrect, please try again.
                    </div>
                ) : null}

                <div>

                    <InputText className="mb-3" label="Email address" value={email} onUpdateValue={setEmail} disabled={loading} />

                    <InputPassword className="mb-10" label="Password" value={password} onUpdateValue={setPassword} disabled={loading} />

                    <PrimaryButton
                        disabled={loading}
                        onClick={doLogin}
                    >
                        Login
                    </PrimaryButton>

                    <div className="mt-10 text-center mb-3">- OR -</div>
                    <div className="text-center">
                        <Link to="register">
                            <div className="text-brand-primary text-lg font-bold">Create an account</div>
                        </Link>
                    </div>

                </div>

            </div>
        </div>
    )
}