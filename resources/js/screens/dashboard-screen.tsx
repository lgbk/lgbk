import React from "react"
import { useTodaysTasks } from "../redux/task/hooks"
import { Task } from "../api/task/task"
import { useWorkspaceById } from "../redux/workspace/hooks"
import { useTaskById, useTaskProjectFinder } from "../redux/task/hooks"
import { TaskList } from "../components/task/task"

export const DashboardScreen: React.FC = () => {
    const tasks = useTodaysTasks();
    const projectFinder = useTaskProjectFinder();

    const projectIds: string[] = [];
    const sortedTasks: any = {};

    tasks.forEach((task) => {
        const project = projectFinder(task);
        if(project){
            if(!projectIds.includes(project.id)){
                projectIds.push(project.id);
                sortedTasks[project.id] = [];
            }
            sortedTasks[project.id].push(task);
        }
    })

    return (
        <>
        <h1 className="mb-10">Todays tasks</h1>
        {projectIds.map((projectId) => {
            return (
                <div key={"project_list_"+projectId}>
                    <TodaysTasksByProject projectId={projectId} tasks={sortedTasks[projectId]} />
                </div>
            )
        })}
        </>
    )
}

type TodaysTasksByProjectProps = {
    projectId: string,
    tasks: Task[]
}

export const TodaysTasksByProject: React.FC<TodaysTasksByProjectProps> = (props) => {
    const project = useTaskById(props.projectId);
    const workspace = useWorkspaceById(project?.workspace_id);

    if(!project || !workspace){
        return null;
    }

    return (
        <div className="mb-5">
            <h2 className="mb-5">{workspace.title+" > "+project.title}</h2>
            <TaskList tasks={props.tasks} showAdd={false} />
        </div>
    )
}

// export const DashboardScreen: React.FC = () => {
//     const tasks = useTodaysTasks();

//     const projectIds: string[] = [];
//     tasks.forEach((task) => {
//         if(task.project_id){
//             if(!projectIds.includes(task.project_id)){
//                 projectIds.push(task.project_id);
//             }
//         }
//     })

//     const filterTasksForProject = (projectId: string): Task[] => {
//         return tasks.filter((task) => task.project_id == projectId);
//     }

//     return (
//         <>
//         <h1 className="mb-10">Todays tasks</h1>
//         {projectIds.map((projectId) => {
//             return (
//                 <div key={"project_list_"+projectId}>
//                     <TodaysTasksByProject projectId={projectId} tasks={filterTasksForProject(projectId)} />
//                 </div>
//             )
//         })}
//         </>
//     )
// }

// type TodaysTasksByProjectProps = {
//     projectId: string,
//     tasks: Task[]
// }

// export const TodaysTasksByProject: React.FC<TodaysTasksByProjectProps> = (props) => {
//     const project = useTaskById(props.projectId);
//     const workspace = useWorkspaceById(project?.workspace_id);

//     if(!project || !workspace){
//         return null;
//     }

//     return (
//         <div className="mb-5">
//             <h2 className="mb-5">{workspace.title+" > "+project.title}</h2>
//             <TaskList tasks={props.tasks} showAdd={false} />
//         </div>
//     )
// }