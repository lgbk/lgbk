import React, { useState } from "react"
// import { useSelector } from "react-redux";
import { useWorkspaceById } from "../../redux/workspace/hooks"
import { useParams } from "react-router-dom"
import { PageNavButton, PageNav } from "../../components/structure/page-nav"
import { SquarePlus, Settings } from "../../components/icons/icons"
import { BreadCrumbNav } from "../../components/structure/breadcrumb-nav"
import { useTaskById, useChildTasksOf } from "../../redux/task/hooks"
import { TaskList } from "../../components/task/task"
import { TaskActions } from "../../components/task/task-actions"
import { BackButton } from "../../components/structure/back-button"

export const ProjectViewScreen: React.FC = () => {
    const { id } = useParams();

    const [showCompleted, setShowCompleted] = useState(false);

    const task = useTaskById(id);
    const workspace = useWorkspaceById(task?.workspace_id);
    const children = useChildTasksOf(task, false, true, showCompleted);

    if(!task || !workspace){
        return null;
    }

    return (
        <div>

            <BreadCrumbNav className="mb-5" crumbs={[
                {
                    text: workspace.title,
                    link: "/workspace/"+workspace.id
                },
                {
                    text: task.title
                }
            ]} />

            <div className="flex flex-row justify-between mb-5 items-center">

                <div className="flex flex-row items-center gap-5">
                    <h1>{task.title}</h1>
                </div>

            </div>

            <div className="border-t border-b py-3 mb-10">
                <TaskActions task={task} showAdd={true} showCompleted={showCompleted} setShowCompleted={setShowCompleted} />
            </div>

            <TaskList tasks={children} recursive={true} showAdd={true} showCompleted={showCompleted} />
            
        </div>
    )
}