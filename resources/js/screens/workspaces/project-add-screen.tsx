import React, { useState } from "react"
import { useSelector, useDispatch } from "react-redux";
import { useWorkspaceById } from "../../redux/workspace/hooks"
import { useParams, useNavigate } from "react-router-dom"
import { BackButton } from "../../components/structure/back-button"
import { InputText } from "../../components/forms/input"
import { PrimaryButton } from "../../components/forms/button"
import { selectTasksLoading, createTask } from "../../redux/task/slice"

export const ProjectAddScreen: React.FC = () => {
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { id } = useParams();

    const [title, setTitle] = useState("");

    const [errorText, setErrorText] = useState("");
    const loading = useSelector(selectTasksLoading);

    if(!id){
        return null;
    }

    const workspace = useWorkspaceById(id);

    if(!workspace){
        return null;
    }

    const doCreateWorkspace = async () => {
        setErrorText("");

        if(!title){
            setErrorText("You must specify a name for your new project");
            return;
        }

        let result = await dispatch(createTask({
            workspace: workspace,
            title: title,
            priority: 3
        }));

        setTitle("");
        
        navigate("/project/"+result.payload.id);
    }

    return (
        <div>

            <div className="flex flex-row mb-5 items-center gap-5">
                <BackButton link={"/workspace/"+workspace.id} />
                <h1>New project under {workspace.title}</h1>
            </div>

            {errorText ? (
                <p className="mb-5 text-red-400">{errorText}</p>
            ) : null}

            <InputText 
                className="mb-5" 
                label="Title"
                placeholder="What is the name of this project?"
                value={title} 
                onUpdateValue={setTitle} 
                disabled={loading} 
            />

            <PrimaryButton
                disabled={loading}
                onClick={doCreateWorkspace}
            >
                Create project
            </PrimaryButton>
            
        </div>
    )
}