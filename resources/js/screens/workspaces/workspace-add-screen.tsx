import React, { useState } from "react"
import { InputText, InputColor } from "../../components/forms/input"
import { useSelector, useDispatch } from "react-redux";
import { selectWorkspacesLoading, createWorkspace } from "../../redux/workspace/slice"
import { PrimaryButton } from "../../components/forms/button"
import { useNavigate } from "react-router-dom"

export const WorkspaceAddScreen: React.FC = () => {
    const loading = useSelector(selectWorkspacesLoading);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [workspaceName, setWorkspaceName] = useState("");
    const [color, setColor] = useState("#4f646f");
    const [errorText, setErrorText] = useState("");

    const doCreateWorkspace = async () => {
        setErrorText("");

        if(!workspaceName){
            setErrorText("You must specify a name for your new workspace");
            return;
        }

        if(!color){
            setErrorText("You must choose a color for your workspace");
            return;
        }

        let result = await dispatch(createWorkspace({title: workspaceName, color: color}));

        setWorkspaceName("");
        setColor("#4f646f");
        
        navigate("/workspace/"+result.payload.id);
    }

    return (
        <div>

            <h1 className="mb-5">Add a new workspace</h1>

            {errorText ? (
                <p className="mb-5 text-red-400">{errorText}</p>
            ) : null}

            <p className="mb-3">Workspaces contain as many projects as you would like, you can use them to separate the different areas of your life.</p>
            <p className="mb-5">For example, perhaps you would have a workspace for your day job, one for your house chores and one for your hobbies.</p>

            <InputText 
                className="mb-3" 
                label="Title"
                placeholder="What do you want to call this workspace?"
                value={workspaceName} 
                onUpdateValue={setWorkspaceName} 
                disabled={loading} 
            />

            <InputColor 
                className="mb-5" 
                label="Color"
                value={color} 
                onUpdateValue={setColor} 
                disabled={loading} 
            />

            <PrimaryButton
                disabled={loading}
                onClick={doCreateWorkspace}
            >
                Create workspace
            </PrimaryButton>



        </div>
    )
}