import React from "react"
// import { useSelector } from "react-redux";
import { useWorkspaceById } from "../../redux/workspace/hooks"
import { useParams } from "react-router-dom"
import { PageNavButton, PageNav } from "../../components/structure/page-nav"
import { SquarePlus, Settings } from "../../components/icons/icons"
import { BreadCrumbNav } from "../../components/structure/breadcrumb-nav"
import { useWorkspaceTasks } from "../../redux/task/hooks"
import { TaskList } from "../../components/task/task"

export const WorkspaceViewScreen: React.FC = () => {
    const { id } = useParams();

    if(!id){
        return null;
    }

    const workspace = useWorkspaceById(id);
    const tasks = useWorkspaceTasks(workspace);

    if(!workspace){
        return null;
    }

    return (
        <div>

            <div className="flex flex-row justify-between mb-5 items-center">

                <h1>{workspace.title}</h1>

                <PageNav>

                    <PageNavButton
                        text="New project"
                        link={"/workspace/"+workspace.id+"/project/add"}
                        icon={<SquarePlus width={18} height={18} className="fill-text-primary" />}
                    />

                    <PageNavButton
                        text="Settings"
                        link={"/workspace/"+workspace.id+"/edit"}
                        icon={<Settings width={18} height={18} className="fill-text-primary" />}
                    />

                </PageNav>

            </div>
            
            <TaskList tasks={tasks} />
            
        </div>
    )
}