import React, { useState } from "react"
import { InputText, InputColor } from "../../components/forms/input"
import { useWorkspaceById } from "../../redux/workspace/hooks"
import { useParams, useNavigate } from "react-router-dom"
import { BackButton } from "../../components/structure/back-button"
import { selectWorkspacesLoading, updateWorkspace, deleteWorkspace } from "../../redux/workspace/slice"
import { PrimaryButton, DeleteIconButton } from "../../components/forms/button"
import { useSelector, useDispatch } from "react-redux";
import { ConfirmModal } from "../../components/modal/modal"

export const WorkspaceEditScreen: React.FC = () => {
    const loading = useSelector(selectWorkspacesLoading);
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const { id } = useParams();

    if(!id){
        return null;
    }

    const workspace = useWorkspaceById(id);

    if(!workspace){
        return null;
    }

    const [workspaceName, setWorkspaceName] = useState(workspace.title);
    const [color, setColor] = useState(workspace.color);
    const [errorText, setErrorText] = useState("");

    const [deleteVisible, setDeleteVisible] = useState(false);

    const doUpdateWorkspace = async () => {
        setErrorText("");

        if(!workspaceName){
            setErrorText("You must specify a name for your workspace");
            return;
        }

        if(!color){
            setErrorText("You must choose a color for your workspace");
            return;
        }

        let result = await dispatch(updateWorkspace({id: workspace.id, title: workspaceName, color: color}));
        navigate("/workspace/"+workspace.id)
    }

    return (
        <div>
            <ConfirmModal
                visible={deleteVisible}
                destructive={true}
                text="Are you sure you want to delete this workspace? Doing so will also delete ALL projects and tasks inside it."
                onConfirm={async () => {
                    setDeleteVisible(false);

                    dispatch(deleteWorkspace({id: workspace.id}));
                    navigate("/")
                }}
                onCancel={() => {
                    setDeleteVisible(false);
                }}
            />

            <div className="flex flex-row mb-5 items-center gap-5 justify-between">

                <div className="flex flex-row items-center gap-5">
                    <BackButton link={"/workspace/"+workspace.id} />
                    <h1>{workspace.title} settings</h1>
                </div>

                <DeleteIconButton size={28} className="fill-red-400" onClick={() => {setDeleteVisible(true)}} />

            </div>

            {errorText ? (
                <p className="mb-5 text-red-400">{errorText}</p>
            ) : null}

            <InputText 
                className="mb-3" 
                label="Title"
                placeholder="What do you want to call this workspace?"
                value={workspaceName} 
                onUpdateValue={setWorkspaceName} 
                disabled={loading} 
            />

            <InputColor 
                className="mb-5" 
                label="Color"
                value={color} 
                onUpdateValue={setColor} 
                disabled={loading} 
            />

            <PrimaryButton
                disabled={loading}
                onClick={doUpdateWorkspace}
            >
                Save
            </PrimaryButton>
            


            
        </div>
    )
}