import React from "react"
import { Outlet } from "react-router-dom"
import { PageContainer } from "../../components/structure/page-container"
import { LargeTextLogo } from "../../components/branding/logo"

export const GuestLayout: React.FC<React.PropsWithChildren> = (props) => {
    return (
        <PageContainer>

            <div className="flex flex-row">

                <div className="w-1/2 shrink-0 h-screen bg-brand-primary flex justify-center items-center">
                    <LargeTextLogo/>
                </div>

                <div className="w-1/2 shrink-0 h-screen bg-white p-10 overflow-y-auto">
                    <Outlet />
                </div>

            </div>

        </PageContainer>
    )
}