import React from "react";
import { PageContainer } from "../../components/structure/page-container"
import { Header } from "../../components/structure/header"
import { Outlet } from "react-router-dom"
import { useMenuIsOpen } from "../../redux/structure/hooks"
import { Menu } from "../../components/structure/menu"

export const LoggedInLayout: React.FC<React.PropsWithChildren> = (props) => {
    const menuIsOpen = useMenuIsOpen();

    return (
        <PageContainer>
            <Header/>

            <div style={{height: "calc(100% - 49px)"}} className="relative">
                
                <Menu/>

                <div className={"absolute top-0 right-0 bottom-0 left-0 left-transition bg-main-background" + (menuIsOpen ? " left-72" : "")}>
                    <div className="p-10 flex justify-center h-full overflow-y-auto">

                        <div style={{maxWidth: "800px"}} className="w-full h-max">
                            <Outlet />
                        </div>
                        
                    </div>
                </div>

            </div>

        </PageContainer>
    )
}