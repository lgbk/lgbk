import React, { useState } from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { store } from "./redux/store";
import { HashRouter, Routes, Route } from "react-router-dom";
import { GuestLayout } from "./screens/layouts/guest"
import { LoggedInLayout } from "./screens/layouts/logged-in"
import { LoginScreen } from "./screens/login-screen"
import { RegisterScreen } from "./screens/register-screen"
import { DashboardScreen } from "./screens/dashboard-screen"
import { WorkspaceAddScreen } from "./screens/workspaces/workspace-add-screen"
import { WorkspaceViewScreen } from "./screens/workspaces/workspace-view-screen"
import { WorkspaceEditScreen } from "./screens/workspaces/workspace-edit-screen"
import { ProjectAddScreen } from "./screens/workspaces/project-add-screen"
import { ProjectViewScreen } from "./screens/workspaces/project-view-screen"
import { TestScreen } from "./screens/test-screen"

const App = () => {
    return (
        <HashRouter>
            <Routes>

                <Route path="/" element={<GuestLayout />}>
                    <Route index element={<LoginScreen/>}/>
                    <Route path="register" element={<RegisterScreen/>}/>
                </Route>

                <Route path="/" element={<LoggedInLayout />}>
                    <Route path="today" element={<DashboardScreen/>}/>
                    <Route path="test" element={<TestScreen/>}/>
                    <Route path="workspace-add" element={<WorkspaceAddScreen/>}/>
                    <Route path="workspace/:id" element={<WorkspaceViewScreen/>}/>
                    <Route path="workspace/:id/edit" element={<WorkspaceEditScreen/>}/>
                    <Route path="workspace/:id/project/add" element={<ProjectAddScreen/>}/>
                    <Route path="project/:id" element={<ProjectViewScreen/>}/>
                </Route>

            </Routes>
        </HashRouter>
    );
};

const AppContainer: React.FC = () => {
    return (
        <React.StrictMode>
            <Provider store={store}>
                <App />
            </Provider>
        </React.StrictMode>
    );
}

const root = ReactDOM.createRoot(document.getElementById('app'));
root.render(<AppContainer />);
