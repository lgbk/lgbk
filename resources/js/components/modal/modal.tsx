import React from "react";
import { PrimaryButton, SecondaryButton, DeleteButton } from "../forms/button"


type ModalBackgroundType = React.PropsWithChildren & {
    onClickOutsideModal?: () => void,
    visible: boolean
}

const ModalBackground: React.FC<ModalBackgroundType> = (props) => {

    if(!props.visible){
        return null;
    }

    return (
        <div 
            className="fixed top-0 right-0 bottom-0 left-0 bg-slate-600/50 flex justify-center items-center"
            onClick={(e) => {
                if(props.onClickOutsideModal){
                    props.onClickOutsideModal();
                }
            }}
        >
            {props.children}
        </div>
    );
}


const ModalBox: React.FC<React.PropsWithChildren> = (props) => {
    return (
        <div 
            className="w-96 p-10 bg-white rounded-lg border-4 border-slate-100"
            onClick={(e) => {e.stopPropagation();}}
        >
            {props.children}
        </div>
    )
}


type GenericModalType = React.PropsWithChildren & {
    visible: boolean,
    onClose: () => void
}

export const GenericModal: React.FC<GenericModalType> = (props) => {
    return (
        <ModalBackground visible={props.visible} onClickOutsideModal={() => {props.onClose()}}>
            <ModalBox>
                {props.children}
            </ModalBox>
        </ModalBackground>
    )
}


type ConfirmModalType = {
    visible: boolean,
    text: string,
    destructive?: boolean,
    onConfirm: () => void,
    onCancel: () => void
}

export const ConfirmModal: React.FC<ConfirmModalType> = (props) => {
    return (
        <GenericModal visible={props.visible} onClose={() => {props.onCancel()}}>
            <div className="mb-8">{props.text}</div>

            <div className="flex flex-row gap-5 justify-between">
                <SecondaryButton onClick={() => {props.onCancel()}}>No</SecondaryButton>

                {props.destructive ? (
                    <DeleteButton onClick={() => {props.onConfirm()}}>Yes</DeleteButton>
                ) : (
                    <PrimaryButton onClick={() => {props.onConfirm()}}>Yes</PrimaryButton>
                )}

            </div>
        </GenericModal>
    )
}