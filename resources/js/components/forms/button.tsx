import React from "react"
import { Cross } from "../icons/icons"

export type ButtonType = React.ButtonHTMLAttributes<HTMLButtonElement> & {
    children?: string
}

export type IconButtonType = React.ButtonHTMLAttributes<HTMLButtonElement> & {
    size: number,
    className?: string,
}

export const PrimaryButton: React.FC<ButtonType> = (props) => {
    return (
        <button
            className={
                "font-bold text-xl w-full rounded-md p-2 border-4 "+
                "bg-white border-brand-primary text-brand-primary "+
                "active:border-brand-secondary active:text-brand-secondary "+
                "disabled:border-brand-secondary disabled:text-brand-secondary disabled:cursor-default"
            }
            {...props}
        >
            {props.children}
        </button>
    );
}


export const SecondaryButton: React.FC<ButtonType> = (props) => {
    return (
        <button
            className={
                "font-bold text-xl w-full rounded-md p-2 border-4 "+
                "bg-white border-gray-400 text-gray-400 "+
                "active:border-gray-700 active:text-gray-700 "+
                "disabled:border-gray-700 disabled:text-gray-700 disabled:cursor-default"
            }
            {...props}
        >
            {props.children}
        </button>
    );
}


export const DeleteButton: React.FC<ButtonType> = (props) => {
    return (
        <button
            className={
                "font-bold text-xl w-full rounded-md p-2 border-4 "+
                "bg-white border-red-400 text-red-400 "+
                "active:border-red-700 active:text-red-700 "+
                "disabled:border-red-700 disabled:text-red-700 disabled:cursor-default"
            }
            {...props}
        >
            {props.children}
        </button>
    );
}

export const DeleteIconButton: React.FC<IconButtonType> = (props) => {
    return (
        <button
            {...props}
        >
            <Cross width={props.size} height={props.size} className={props.className} />
        </button>
    );
}