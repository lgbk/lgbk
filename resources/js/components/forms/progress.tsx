import React from "react";

type ProgressProps = {
    value: number,
    max: number,
    className?: string
}

export const ProgressBar: React.FC<ProgressProps> = (props) => {
    const percent = (props.value / props.max) * 100;

    return (
        <div className={props.className}>
            <div
                className="border rounded-full w-full"
                style={{height: "12px", minWidth: "180px"}}
            >
                <div className="h-full rounded-full bg-brand-primary" style={{width: String(percent)+"%"}}></div>
            </div>

        </div>
    )
}