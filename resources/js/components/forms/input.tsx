import React, { useState } from "react"


type InputContainerType = React.PropsWithChildren & {
    label?: string,
    className?: string,
}

export const InputContainer: React.FC<InputContainerType> = (props) => {
    return (
        <div className={"flex flex-col gap-1 " + (props.className ? props.className : "")}>

            {props.label ? (
                <label className="">
                    {props.label}
                </label>
            ) : null}

            {props.children}

        </div>
    )
}


export type InputType = InputContainerType & React.InputHTMLAttributes<HTMLInputElement> & {
    onUpdateValue?: (value: string) => void
}

export const InputText: React.FC<InputType> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>
            <input 
                className="border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                type="text" 
                onChange={(e) => {
                    if(props.onUpdateValue){
                        props.onUpdateValue(e.currentTarget.value);
                    }
                }}
                value={props.value}
                placeholder={props.placeholder}
                disabled={props.disabled}
            />
        </InputContainer>
    )
}

export const InputNumber: React.FC<InputType> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>
            <input 
                className="border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                type="number" 
                onChange={(e) => {
                    if(props.onUpdateValue){
                        props.onUpdateValue(e.currentTarget.value);
                    }
                }}
                value={props.value}
                placeholder={props.placeholder}
                disabled={props.disabled}
                min={props.min}
                max={props.max}
                step={props.step}
            />
        </InputContainer>
    )
}

export const InputPassword: React.FC<InputType> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>
            <input 
                className="border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                type="password" 
                onChange={(e) => {
                    if(props.onUpdateValue){
                        props.onUpdateValue(e.currentTarget.value);
                    }
                }}
                value={props.value}
                disabled={props.disabled}
            />
        </InputContainer>
    )
}

export const InputColor: React.FC<InputType> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>
            <input 
                className="bg-white border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                type="color" 
                onChange={(e) => {
                    if(props.onUpdateValue){
                        props.onUpdateValue(e.currentTarget.value);
                    }
                }}
                value={props.value}
                disabled={props.disabled}
            />
        </InputContainer>
    )
}

export const InputDate: React.FC<InputType> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>
            <input 
                className="border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                type="date" 
                onChange={(e) => {
                    if(props.onUpdateValue){
                        props.onUpdateValue(e.currentTarget.value);
                    }
                }}
                value={props.value}
                disabled={props.disabled}
            />
        </InputContainer>
    )
}

export const InputTime: React.FC<InputType> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>
            <input 
                className="border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                type="time" 
                onChange={(e) => {
                    if(props.onUpdateValue){
                        props.onUpdateValue(e.currentTarget.value);
                    }
                }}
                value={props.value}
                disabled={props.disabled}
            />
        </InputContainer>
    )
}


type InputCheckboxProps = InputContainerType & React.InputHTMLAttributes<HTMLInputElement> & {
    onUpdateValue: (value: boolean) => void,
    isChecked: boolean
}

export const InputCheckbox: React.FC<InputCheckboxProps> = (props) => {
    return (
        <div className={"flex flex-row gap-3"+(props.className ? " "+props.className : "")}>

            <input 
                type="checkbox" 
                onChange={(e) => props.onUpdateValue(e.target.checked)} 
                checked={props.isChecked} 
                disabled={props.disabled}
            />

            <div>{props.label}</div>

        </div>
    )
}


export type InputSelectOption = {
    value: string,
    label: string,
    children?: InputSelectOption[]
}

type InputSelectProps = InputContainerType & React.InputHTMLAttributes<HTMLInputElement> & {
    onUpdateValue: (value: string) => void,
    options: InputSelectOption[],
    showHierarchy?: boolean
}

export const InputSelect: React.FC<InputSelectProps> = (props) => {
    return (
        <InputContainer label={props.label} className={props.className}>

            <select
                disabled={props.disabled}
                className="bg-white border-2 rounded-sm p-1 border-slate-300 focus:border-brand-secondary"
                onChange={(e) => props.onUpdateValue(e.target.value)}
                value={props.value}
            >
                {props.options.map((option, index) => {
                    return (
                        <InputSelectOption 
                            value={option.value} 
                            label={option.label} 
                            children={option.children} 
                            depth={0}
                            isLastInList={index == (props.options.length - 1)}
                            showHierarchy={props.showHierarchy}
                        />
                    )
                })}
            </select>

        </InputContainer>
    )
}

const InputSelectOption: React.FC<InputSelectOption & {depth: number, isLastInList: boolean, showHierarchy?: boolean}> = (props) => {
    let depthPrefix = "";
    if(props.showHierarchy){
        if(props.depth > 0){depthPrefix = depthPrefix + ("│ ".repeat(props.depth))}

        if(!props.isLastInList){
            depthPrefix = depthPrefix + "├ "
        } else {
            depthPrefix = depthPrefix + "└ "
        }
    }

    return (
        <>
            <option
                key={props.value}
                value={props.value}
            >
                {depthPrefix+props.label}
            </option>

            {props.children ? (
                props.children.map((child, index, allChildren) => {
                    return <InputSelectOption 
                        value={child.value} 
                        label={child.label} 
                        children={child.children} 
                        depth={props.depth + 1}
                        isLastInList={index == (allChildren.length - 1)}
                        showHierarchy={props.showHierarchy}
                    />
                })
            ) : null}
            
        </>
    )
}