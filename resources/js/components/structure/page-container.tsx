import React from "react";

type PageContainerType = React.PropsWithChildren & {};

export const PageContainer: React.FC<PageContainerType> = (props) => {
    return (
        <div className="w-screen h-screen overflow-hidden bg-main-background">
            {props.children}
        </div>
    )
}