import React from "react"
import { MenuButton } from "./menu-button"
import { HomeButton } from "./home-button"
import { TextLogo } from "../branding/logo"

export const Header: React.FC = () => {
    return (
        <div className="px-8 py-2 w-full flex justify-between flex-row bg-brand-primary" style={{height: "48px"}}>
            <HeaderLeft/>
            <HeaderRight/>
        </div>
    )
}

const HeaderLeft: React.FC = () => {
    return (
        <div className="w-1/2 flex flex-row shrink-0 gap-3 items-center">
            <TextLogo/>
        </div>
    )
}

const HeaderRight: React.FC = () => {
    return (
        <div className="w-1/2 flex flex-row-reverse shrink-0 gap-4 items-center">
            <MenuButton />
            <HomeButton />
        </div>
    )
}