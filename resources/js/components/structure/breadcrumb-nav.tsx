import React from "react"
import { Link } from "react-router-dom"

type BreadCrumbNavButtonType = {
    text: string,
    link?: string
}

type BreadCrumbNavType = {
    crumbs: BreadCrumbNavButtonType[],
    className?: string
}

export const BreadCrumbNav: React.FC<BreadCrumbNavType> = (props) => {
    return (
        <div className={"flex flex-row gap-1"+(props.className ? " "+props.className : "")}>
            {props.crumbs.map((crumb, index, all) => {
                return (
                    <div className="flex flex-row gap-1" key={"breadcrumb_"+String(index)}>
                        {index > 0 && index < all.length ? (
                            <div className="text-sm">/</div>
                        ) : null}
                        <BreadCrumbNavButton text={crumb.text} link={crumb.link} />
                    </div>
                )
            })}
        </div>
    );
}

const BreadCrumbNavButton: React.FC<BreadCrumbNavButtonType> = (props) => {
    return (
        <div>
            {props.link ? (
                <Link to={props.link}>
                    <div className="text-sm text-brand-primary underline cursor-pointer">
                        {props.text}
                    </div>
                </Link>
            ) : (
                <div className="text-sm text-primary">
                    {props.text}
                </div>
            )}
        </div>
    );
}