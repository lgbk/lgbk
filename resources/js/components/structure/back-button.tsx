import React from "react"
import { Link } from "react-router-dom"
import { BackArrow } from "../icons/icons"

type BackButtonType = {
    link: string,
    className?: string
}

export const BackButton: React.FC<BackButtonType> = (props) => {
    return (
        <Link to={props.link}>
            <div style={{width: "48px", height: "48px"}} className="flex justify-center items-center">
                <BackArrow 
                    width={24} 
                    height={24} 
                    className={"fill-brand-primary" + (props.className ? " "+props.className : "")}
                />
            </div>
        </Link>
    )
}