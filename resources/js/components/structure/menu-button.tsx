import React from "react";
import { useMenuIsOpen } from "../../redux/structure/hooks"
import { setMenuIsOpen } from "../../redux/structure/slice"
import { useDispatch } from "react-redux"
import { MenuIcon } from "../icons/icons"

export const MenuButton: React.FC = () => {
    const dispatch = useDispatch();
    const menuIsOpen = useMenuIsOpen();

    return (
        <div className="cursor-pointer fill-white" onClick={() => {
            if(menuIsOpen){
                dispatch(setMenuIsOpen({isOpen: false}))
            } else {
                dispatch(setMenuIsOpen({isOpen: true}))
            }
        }}>
            <MenuIcon width={24} height={24} className="fill-white"/>
        </div>
    )
}