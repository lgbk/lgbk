import React from "react";
import { useNavigate, useLocation } from "react-router-dom"
import { useMenuIsOpen } from "../../redux/structure/hooks"
import { HomeIcon, SquarePlus, Logout } from "../icons/icons"
import { useWorkspaces } from "../../redux/workspace/hooks"
import { useWorkspaceTasks } from "../../redux/task/hooks"
import { Workspace } from "../../api/workspace/workspace"
import { logout } from "../../api/auth/auth"

export const Menu: React.FC = () => {
    const menuIsOpen = useMenuIsOpen();
    const workspaces = useWorkspaces();
    const navigate = useNavigate();

    return (
        <div className="h-full absolute w-72 bg-side-background top-0 left-0 overflow-y-auto px-5 py-8">
            
            <MenuButton 
                text="Todays tasks" 
                icon={<HomeIcon width={18} height={18} className="fill-text-primary" />}
                link="/today"
            />

            <MenuButton 
                text="Add new workspace"
                icon={<SquarePlus width={18} height={18} className="fill-text-primary" />}
                link="/workspace-add"
            />

            {workspaces.length > 0 ? (
                <>
                    <div className="mt-5 mb-3 border-2 border-dashed border-side-background-detail w-full"></div>

                    {workspaces.map((workspace, index) => {
                        return (
                            <div key={"workspace_menu_btn__"+String(index)}>
                                <WorkspaceMenuButtons workspace={workspace} />
                            </div>
                        )
                    })}
                </>
            ) : null}

            <div className="mt-3 mb-5 border-2 border-dashed border-side-background-detail w-full"></div>

            <MenuButton 
                text="Log out"
                icon={<Logout width={18} height={18} className="fill-text-primary" />}
                onClick={async () => {
                    await logout();
                    navigate("/");
                }}
            />

        </div>
    )
}


type MenuButtonType = {
    text: string
    icon?: React.ReactNode
    color?: string
    link?: string
    onClick?: () => void
}

export const MenuButton: React.FC<MenuButtonType> = (props) => {
    const navigate = useNavigate();
    const location = useLocation();

    let isActive = props.link ? location.pathname.startsWith(props.link) : false;
    if(props.link == "/"){
        isActive = location.pathname == props.link;
    }

    return (
        <div 
            className={
                "flex flex-row gap-3 cursor-pointer items-center mb-1 p-2 rounded-md"+
                " hover:bg-side-background-detail" + (isActive ? " bg-side-background-detail" : "")}
            onClick={() => {
                if(props.link){
                    navigate(props.link);
                } else if(props.onClick){
                    props.onClick();
                }
            }}
        >

            <div>
                {props.icon? (
                    props.icon
                ) : (
                    <MenuButtonColorCircle color={props.color} />
                )}
            </div>

            <div className="">
                {props.text}
            </div>

        </div>
    );
}


type MenuButtonColorCircleType = {
    color?: string
}

export const MenuButtonColorCircle: React.FC<MenuButtonColorCircleType> = (props) => {
    let useColor = props.color
    if(!useColor){
        useColor = "#4f646f";
    }

    return (
        <div 
            style={{width: "10px", height: "10px", backgroundColor: useColor}}
            className="rounded-full"
        ></div>
    );
}



type WorkspaceMenuButtonsProps = {
    workspace: Workspace
}

export const WorkspaceMenuButtons: React.FC<WorkspaceMenuButtonsProps> = (props) => {
    const tasks = useWorkspaceTasks(props.workspace);

    return (
        <div>
            <MenuButton 
                text={props.workspace.title}
                color={props.workspace.color}
                link={"/workspace/"+props.workspace.id}
            />
            {tasks.map((task, index) => {
                return (
                    <div key={"project_menu_"+String(index)} className="ml-5 text-sm">
                        <MenuButton 
                            text={task.title}
                            color={props.workspace.color}
                            link={"/project/"+task.id}
                        />
                    </div>
                )
            })}
        </div>
    );
}