import React from "react"
import { useNavigate } from "react-router-dom"

export const PageNav: React.FC<React.PropsWithChildren> = (props) => {
    return (
        <div className="flex flex-row gap-3">
            {props.children}
        </div>
    );
}

type PageNavButtonType = {
    text?: string
    icon?: React.ReactNode
    link?: string
    onClick?: () => void
}

export const PageNavButton: React.FC<PageNavButtonType> = (props) => {
    const navigate = useNavigate();

    return (
        <div 
            className={
                "flex flex-row gap-3 items-center p-1 rounded-md"+
                (props.onClick ? " hover:bg-side-background cursor-pointer" : "")}
            onClick={() => {
                if(props.link){
                    navigate(props.link);
                } else {
                    if(props.onClick){
                        props.onClick();
                    }
                }
            }}
        >

            <div>
                {props.icon? (
                    props.icon
                ) : null}
            </div>

            {props.text ? (
                <div className="">
                    {props.text}
                </div>
            ) : null}
            
        </div>
    );
}