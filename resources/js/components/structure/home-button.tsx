import React from "react";
import { HomeIcon } from "../icons/icons"
import { useNavigate } from "react-router-dom"

export const HomeButton: React.FC = () => {
    const navigate = useNavigate();

    return (
        <div className="cursor-pointer fill-white" onClick={() => {
            navigate("/");
        }}>
            <HomeIcon width={22} height={22} className="fill-white"/>
        </div>
    )
}