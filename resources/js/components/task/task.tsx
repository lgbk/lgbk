import React, { useState } from "react";
import { Task as TaskType, TaskPriority, TaskTimeMode } from "../../api/task/task"
import { dateToDateTimeString } from "../../api/api"
import { Check, Lock, TurnDown } from "../icons/icons"
import { useTaskEstimateString, useChildTasksOf, useTaskUpdater, useTaskLineage } from "../../redux/task/hooks"
import { useNavigate } from "react-router-dom";
import { TaskActions } from "./task-actions"


type TaskListProps = {
    tasks: TaskType[],
    className?: string,
    recursive?: boolean,
    showAdd?:boolean,
    showCompleted?:boolean,
}

export const TaskList: React.FC<TaskListProps> = (props) => {
    return (
        <div className={props.className}>
            {props.tasks.map((task, index, tasks) => {
                return (
                    <div key={task.id} className={"py-3 "+(tasks.length > 1 && index < (tasks.length - 1) ? "border-b" : "")}>
                        <Task task={task} recursive={props.recursive} showAdd={props.showAdd} showCompleted={props.showCompleted} />
                    </div>
                )
            })}
        </div>
    )
}


type TaskProps = {
    task: TaskType,
    className?: string,
    recursive?: boolean,
    showAdd?:boolean,
    showCompleted?:boolean,
}

export const Task: React.FC<TaskProps> = (props) => {
    const navigate = useNavigate();
    const eta = useTaskEstimateString(props.task);
    const children = useChildTasksOf(props.task, false, true, props.showCompleted);
    const [childrenOpen, setChildrenOpen] = useState(false);
    const updateTask = useTaskUpdater(props.task);
    const lineage = useTaskLineage(props.task, false);
    const now = new Date();

    const toggleTaskComplete = async () => {
        if(props.task.complete){
            await updateTask({complete: false})
        } else {
            await updateTask({complete: true})
        }
    }

    const taskClicked = () => {
        if(!props.task.parent_id){
            navigate("/project/"+props.task.id);
        } else if(children.length > 0){
            setChildrenOpen(!childrenOpen);
        }
    }

    return (
        <div className={props.className}>

            <div className="flex flex-row gap-5">

                {props.task.parent_id ? (
                    props.task.has_children ? (
                        <div className="cursor-pointer" style={{width: "24px", height: "24px"}} onClick={() => {
                            setChildrenOpen(!childrenOpen);
                        }}>
                            <TurnDown 
                                width={24} 
                                height={24} 
                                className={(childrenOpen ? "fill-brand-primary" : "fill-gray-400")}
                            />
                        </div>
                    ) : (
                        props.task.blocked ? (
                            <Lock width={24} height={24} className="fill-text-primary" />
                        ) : (
                            <TaskCheckButton 
                            isChecked={props.task.complete}
                            priority={props.task.priority}
                            onClick={toggleTaskComplete}
                        />
                        )
                    )
                ) : null}

                <div className="flex-grow">

                    {lineage.length > 0 && !props.recursive ? (
                        <div className="flex flex-row gap-2 mb-1">
                            {lineage.map((item, index) => {
                                return (
                                    <div key={item.id} className="flex flex-row gap-2 text-xs">
                                        {index >= 1 ? (<div>{">"}</div>) : null}
                                        <div className="underline">{item.title}</div>
                                    </div>
                                )
                            })}
                        </div>
                    ) : null}

                    <div onClick={taskClicked} className={(!props.task.parent_id || children.length > 0 ? "cursor-pointer" : "")}>
                        {props.task.title}
                    </div>

                    {eta ? (
                        <div className="pt-1 text-xs">{children.length > 0 ? "Remaining" : "Estimate"}: {eta}</div>
                    ) : null}

                    {props.task.time_mode != TaskTimeMode.None ? (
                        <>
                            {props.task.time_mode == TaskTimeMode.DueBy && props.task.end_time ? (
                                <div className={"pt-1 text-xs"+(props.task.end_time < now ? " text-red-500 font-bold" : "")}>
                                    Due by: {dateToDateTimeString(props.task.end_time)}
                                </div>
                            ) : null}

                            {props.task.time_mode == TaskTimeMode.StartAt && props.task.start_time ? (
                                <div className={"pt-1 text-xs"+(props.task.start_time < now ? " text-red-500 font-bold" : "")}>
                                    Starts at: {dateToDateTimeString(props.task.start_time)}
                                </div>
                            ) : null}
                        </>
                    ) : null}

                    <div className="pt-3">
                        <TaskActions task={props.task} showAdd={props.showAdd} />
                    </div>

                </div>

            </div>


            {props.recursive && children.length > 0 && childrenOpen ? (
                <div className="flex flex-col ml-10 border-t mt-5">
                    <TaskList tasks={children} recursive={props.recursive} showAdd={props.showAdd} showCompleted={props.showCompleted} />
                </div>
            ) : null}


        </div>
    )
}


type TaskCheckButtonProps = {
    isChecked: boolean,
    priority: TaskPriority,
    onClick: () => void
}

const TaskCheckButton: React.FC<TaskCheckButtonProps> = (props) => {

    let useBorder = "border-text-primary";
    if(!props.isChecked){
        if(props.priority == 1){
            useBorder = "border-priority-very-low";
        } else if(props.priority == 2){
            useBorder = "border-priority-low";
        } else if(props.priority == 3){
            useBorder = "border-priority-normal";
        } else if(props.priority == 4){
            useBorder = "border-priority-high";
        } else if(props.priority == 5){
            useBorder = "border-priority-very-high";
        }
    }
    useBorder = " "+useBorder

    let useBackground = "bg-white";
    if(props.isChecked){
        if(props.priority == 1){
            useBackground = "bg-priority-very-low";
        } else if(props.priority == 2){
            useBackground = "bg-priority-low";
        } else if(props.priority == 3){
            useBackground = "bg-priority-normal";
        } else if(props.priority == 4){
            useBackground = "bg-priority-high";
        } else if(props.priority == 5){
            useBackground = "bg-priority-very-high";
        }
    }
    useBackground = " "+useBackground

    let useHover = "";
    if(!props.isChecked){
        if(props.priority == 1){
            useHover = " hover:bg-priority-very-low";
        } else if(props.priority == 2){
            useHover = " hover:bg-priority-low";
        } else if(props.priority == 3){
            useHover = " hover:bg-priority-normal";
        } else if(props.priority == 4){
            useHover = " hover:bg-priority-high";
        } else if(props.priority == 5){
            useHover = " hover:bg-priority-very-high";
        }
    }


    return (
        <div 
            onClick={props.onClick}
            style={{width: "24px", height: "24px"}}
            className={"flex flex-row justify-center items-center rounded-full border cursor-pointer"+useBorder+useBackground+useHover}
        >
            <Check width={14} height={14} className="fill-white" />
        </div>
    )
}