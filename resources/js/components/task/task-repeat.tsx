import React, { useState, useEffect } from "react";
import { Task, TaskRepeatMode } from "../../api/task/task"
// import { useTaskFinder } from "../../redux/task/hooks"
import { InputCheckbox } from "../forms/input"


type TaskRepetitionSelectorProps = {
    onSelect: (mode: TaskRepeatMode, config: number[]) => void,
    task: Task
}

export const TaskRepetitionSelector: React.FC<TaskRepetitionSelectorProps> = (props) => {
    const [repeatMode, setRepeatMode] = useState<TaskRepeatMode>(props.task.repeat_mode);
    const [repeatConfig, setRepeatConfig] = useState<number[]>(props.task.repeat_config);

    const modeOptions: {key: TaskRepeatMode, label: string}[] = [
        {key: 0, label: "Off"},
        {key: 1, label: "Week days"},
        {key: 2, label: "Month days"},
        {key: 3, label: "Year months"}
    ]

    useEffect(() => {
        props.onSelect(repeatMode, repeatConfig);
    }, [repeatMode, repeatConfig])

    return (
        <div>

            <div className="flex flex-row rounded-lg border">
                {modeOptions.map((item, index) => {
                    return (
                        <div 
                            key={"task_date_mode_selector__"+String(index)} 
                            className={"flex text-center justify-center items-center w-1/4 cursor-pointer py-1"+(repeatMode == item.key ? " bg-brand-primary" : "")}
                            onClick={() => {
                                setRepeatMode(item.key)
                                setRepeatConfig([]);
                            }}
                        >
                            {item.label}
                        </div>
                    )
                })}
            </div>

            {repeatMode != 0 ? (
                <div className="mt-5">
                    <TaskRepetitionConfigSelector repeatMode={repeatMode} setConfig={(config) => {
                        setRepeatConfig(config);
                    }} currentConfig={repeatConfig} />
                </div>
            ) : null}

        </div>
    )
}


type TaskRepetitionConfigSelectorProps = {
    repeatMode: TaskRepeatMode,
    setConfig: (config: number[]) => void,
    currentConfig: number[]
}

const TaskRepetitionConfigSelector: React.FC<TaskRepetitionConfigSelectorProps> = (props) => {
    let options: {value: number, label: string}[] = [];

    const getMonthDayLabel = (day: number): string => {
        let dayString = String(day);
        let lastChar = dayString.charAt(dayString.length-1);

        if(day >= 10 && day <= 19){
            dayString = dayString + "th";
        } else {
            if(lastChar == "1"){
                dayString = dayString + "st";
            } else if(lastChar == "2"){
                dayString = dayString + "nd";
            } else if(lastChar == "3"){
                dayString = dayString + "rd";
            } else {
                dayString = dayString + "th";
            }
        }

        return dayString;
    }

    if(props.repeatMode == 0){
        return null;
    }

    if(props.repeatMode == 1){
        // Week days
        options = [
            {value: 1, label: "Monday"},
            {value: 2, label: "Tuesday"},
            {value: 3, label: "Wednesday"},
            {value: 4, label: "Thursday"},
            {value: 5, label: "Friday"},
            {value: 6, label: "Saturday"},
            {value: 7, label: "Sunday"}
        ]
    } else if(props.repeatMode == 2){
        // Month days
        options = []
        let di = 1;
        while(di <= 31){
            options.push({value: di, label: getMonthDayLabel(di)});
            di = di + 1
        }
    } else if(props.repeatMode == 3){
        // Year months
        options = [
            {value: 1, label: "January"},
            {value: 2, label: "February"},
            {value: 3, label: "March"},
            {value: 4, label: "April"},
            {value: 5, label: "May"},
            {value: 6, label: "June"},
            {value: 7, label: "July"},
            {value: 8, label: "August"},
            {value: 9, label: "September"},
            {value: 10, label: "October"},
            {value: 11, label: "November"},
            {value: 12, label: "December"}
        ]
    }

    return (
        <div>

            <div className="border rounded-lg overflow-y-auto p-5" style={{maxHeight: "350px"}}>
                {options.map((option, index) => {
                    return(
                        <div key={"option_"+String(index)}>
                            <InputCheckbox
                                isChecked={props.currentConfig.includes(option.value)}
                                label={option.label}
                                onUpdateValue={(isChecked) => {
                                    if(!isChecked){
                                        props.setConfig(props.currentConfig.filter((value) => value != option.value));
                                    } else {
                                        let newConfig: number[] = [...props.currentConfig];
                                        newConfig.push(option.value);
                                        props.setConfig(newConfig);
                                    }
                                    
                                }}
                            />
                        </div>
                    )
                })}
            </div>

        </div>
    )
}