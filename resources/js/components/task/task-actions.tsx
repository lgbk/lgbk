import React, { useState } from "react";
import { Task as TaskType, TaskPriority, TaskTimeMode } from "../../api/task/task"
import { SquarePlus, Flag, Pen, BusinessTime, Timeline, Calendar, Trash, Repeat, MoveInList, Visible, Hidden } from "../icons/icons"
import { useTaskIsParent, useChildTasksOf, useTaskUpdater, useTasksById, useUpdateTaskDependencies, useDeleteTask, useTaskById } from "../../redux/task/hooks"
import { selectTasksLoading, createTask } from "../../redux/task/slice"
import { ProgressBar } from "../forms/progress"
import { GenericModal, ConfirmModal } from "../modal/modal"
import { PageNavButton, PageNav } from "../structure/page-nav"
import { InputText, InputNumber, InputCheckbox, InputSelect } from "../forms/input"
import { PrimaryButton } from "../forms/button"
import { useSelector, useDispatch } from "react-redux";
import { useWorkspaceById } from "../../redux/workspace/hooks"
import { TaskSelector, ListedTask } from "../task/task-selector"
import { TaskRepetitionSelector } from "../task/task-repeat"
import { TaskDateInput } from "../task/task-date-input"
import { TaskPositionSelector } from "../task/task-position"


type TaskActionsProps = {
    task: TaskType,
    showAdd?: boolean
    showCompleted?: boolean
    setShowCompleted?: (show: boolean) => void
}

export const TaskActions: React.FC<TaskActionsProps> = (props) => {
    const isParent = useTaskIsParent(props.task);

    if(isParent){
        return <TaskActionParent {...props} />
    } else {
        return <TaskActionNormal {...props} />
    }
}


const TaskActionParent: React.FC<TaskActionsProps> = (props) => {
    const children = useChildTasksOf(props.task, true, false);

    let completed: number = 0;
    for(let i in children){
        if(children[i].complete){
            completed = completed + 1;
        }
    }

    const percent = children.length > 0 ? ((completed / children.length) * 100) : 0

    return (
        <div className="flex flex-row gap-5 items-center justify-between">

            <div className="flex flex-row gap-5 items-center">

                <ProgressBar max={100} value={percent} />

                <div className="text-xs">
                    {completed} / {children.length} completed
                </div>

            </div>

            <PageNav>

                {props.setShowCompleted ? (
                    <TaskShowCompletedToggle showCompleted={props.showCompleted} setShowCompleted={props.setShowCompleted} />
                ) : null}

                <TaskPriorityButton task={props.task} displayOnly={true} />
                <TaskDependencyButton task={props.task} />
                <TaskMoveButton task={props.task} />
                <TaskDeleteButton task={props.task} />
                <TaskEditButton task={props.task} />

                {props.showAdd ? (
                    <TaskAddButton task={props.task} />
                ) : null}

            </PageNav>

        </div>
    )
}


const TaskActionNormal: React.FC<TaskActionsProps> = (props) => {
    return (
        <div className="flex flex-row-reverse gap-5">
            <PageNav>

                <TaskPriorityButton task={props.task} />

                <TaskDependencyButton task={props.task} />
                <TaskDueButton task={props.task} />
                <TaskEstimateButton task={props.task} />
                <TaskRepeatButton task={props.task} />
                <TaskMoveButton task={props.task} />
                <TaskDeleteButton task={props.task} />
                <TaskEditButton task={props.task} />
                
                {props.showAdd ? (
                    <TaskAddButton task={props.task} />
                ) : null}

            </PageNav>
        </div>
    )
}


const TaskAddButton: React.FC<{task: TaskType}> = (props) => {
    const dispatch = useDispatch();

    const [visible, setVisible] = useState(false);
    const [newTask, setNewTask] = useState("");
    const [errorText, setErrorText] = useState("");
    const workspace = useWorkspaceById(props.task.workspace_id);

    const loading = useSelector(selectTasksLoading);;

    const doAddTask = async () => {
        setErrorText("");

        if(!newTask){
            setErrorText("You must specify what the new task is");
            return;
        }

        let result = await dispatch(createTask({
            workspace: workspace,
            title: newTask,
            priority: 3,
            parent: props.task.id
        }));

        setNewTask("");
        
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Add task</h2>

                {errorText ? (
                    <p className="mb-5 text-red-400">{errorText}</p>
                ) : null}

                <InputText 
                    className="mb-5" 
                    label="Task"
                    placeholder="Brief description of new task"
                    value={newTask} 
                    onUpdateValue={setNewTask} 
                    disabled={loading} 
                />

                <PrimaryButton
                    disabled={loading}
                    onClick={doAddTask}
                >
                    Add task
                </PrimaryButton>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<SquarePlus width={18} height={18} className="fill-text-primary" />}
            />
        </div>
    )
}


const TaskPriorityButton: React.FC<{task: TaskType, displayOnly?: boolean}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);

    let usePriorityColor = "fill-text-primary";
    if(props.task.priority == 1){
        usePriorityColor = "fill-priority-very-low";
    } else if(props.task.priority == 2){
        usePriorityColor = "fill-priority-low";
    } else if(props.task.priority == 4){
        usePriorityColor = "fill-priority-high";
    } else if(props.task.priority == 5){
        usePriorityColor = "fill-priority-very-high";
    }

    const doSetPriority = async (newPriority: TaskPriority) => {
        await updateTask({priority: newPriority});
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Set priority</h2>

                <div>

                    <div className="flex flex-row justify-between">
                        <div 
                            style={{width: "24px", height: "24px"}} 
                            className="cursor-pointer flex justify-center items-center" 
                            onClick={() => {doSetPriority(1)}}
                        >
                            <Flag width={24} height={24} className="fill-priority-very-low" />
                        </div>
                        <div 
                            style={{width: "24px", height: "24px"}} 
                            className="cursor-pointer flex justify-center items-center" 
                            onClick={() => {doSetPriority(2)}}
                        >
                            <Flag width={24} height={24} className="fill-priority-low" />
                        </div>
                        <div 
                            style={{width: "24px", height: "24px"}} 
                            className="cursor-pointer flex justify-center items-center" 
                            onClick={() => {doSetPriority(3)}}
                        >
                            <Flag width={24} height={24} className="fill-text-primary" />
                        </div>
                        <div 
                            style={{width: "24px", height: "24px"}} 
                            className="cursor-pointer flex justify-center items-center" 
                            onClick={() => {doSetPriority(4)}}
                        >
                            <Flag width={24} height={24} className="fill-priority-high" />
                        </div>
                        <div 
                            style={{width: "24px", height: "24px"}} 
                            className="cursor-pointer flex justify-center items-center" 
                            onClick={() => {doSetPriority(5)}}
                        >
                            <Flag width={24} height={24} className="fill-priority-very-high" />
                        </div>
                    </div>

                    <div className="flex flex-row justify-between mt-3 text-xs">
                        <div>Low</div>
                        <div>Normal</div>
                        <div>High</div>
                    </div>

                </div>

            </GenericModal>

            <PageNavButton
                onClick={!props.displayOnly ? () => setVisible(true) : undefined}
                icon={<Flag width={18} height={18} className={usePriorityColor} />}
            />
        </div>
    )
}



const TaskEditButton: React.FC<{task: TaskType}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const [title, setTitle] = useState(props.task.title);
    const [errorText, setErrorText] = useState("");
    const loading = useSelector(selectTasksLoading);

    const doUpdateTitle = async () => {
        setErrorText("");

        if(!title){
            setErrorText("You must enter the description for this task");
            return;
        }

        await updateTask({title: title});
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Update task</h2>

                {errorText ? (
                    <p className="mb-5 text-red-400">{errorText}</p>
                ) : null}

                <InputText 
                    className="mb-5" 
                    label="Task"
                    placeholder="Brief description of task"
                    value={title} 
                    onUpdateValue={setTitle} 
                    disabled={loading} 
                />

                <PrimaryButton
                    disabled={loading}
                    onClick={doUpdateTitle}
                >
                    Update task
                </PrimaryButton>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<Pen width={18} height={18} className="fill-text-primary" />}
            />
        </div>
    )
}



const TaskEstimateButton: React.FC<{task: TaskType}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);

    let useInitialHours = 0;
    let useInitialMins = 0;
    if(props.task.estimate){
        useInitialHours = Math.floor(props.task.estimate / 60)
        useInitialMins = props.task.estimate - (useInitialHours * 60);
    }

    const [hours, setHours] = useState(useInitialHours);
    const [minutes, setMinutes] = useState(useInitialMins);

    const doUpdateEstimate = async () => {
        const estimate = (hours * 60) + minutes;
        await updateTask({estimate: estimate});
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Set time estimate</h2>

                <InputNumber
                    value={hours}
                    onUpdateValue={(value) => setHours(value ? parseInt(value) : 0)}
                    label="Hours"
                    min={0}
                    max={8}
                />

                <InputNumber
                    value={minutes}
                    onUpdateValue={(value) => setMinutes(value ? parseInt(value) : 0)}
                    label="Minutes"
                    min={0}
                    max={59}
                />

                <div className="mt-5">
                    <PrimaryButton
                        disabled={loading}
                        onClick={doUpdateEstimate}
                    >
                        Set estimate
                    </PrimaryButton>
                </div>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<BusinessTime width={18} height={18} className="fill-text-primary" />}
            />
        </div>
    )
}



const TaskDependencyButton: React.FC<{task: TaskType}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);
    const updateDependencies = useUpdateTaskDependencies(props.task);

    const [selected, setSelected] = useState(props.task.blocked_by ? props.task.blocked_by.map((related) => {return related.id}) : []);
    const selectedTasks = useTasksById(selected);

    const doUpdateDependency = async () => {
        await updateDependencies(selectedTasks);
        setVisible(false);
    }

    const addSelection = (t: TaskType) => {
        if(!selected.includes(t.id)){
            setSelected((alreadySelected) => [...alreadySelected, t.id])
        }
    }

    const removeSelection = (t: TaskType) => {
        setSelected((alreadySelected) => alreadySelected.filter((id) => {
            return id != t.id
        }))
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Dependencies</h2>

                <TaskSelector onSelect={addSelection} />

                {selectedTasks.length > 0 ? (
                    <div className="mt-5">

                        <div className="font-bold mb-3">Currently depends on:</div>

                        {selectedTasks.map((selectedTask, index) => {
                            return (
                                <div key={"selected_task_"+selectedTask.id}>
                                    <ListedTask task={selectedTask} onClick={() => {
                                        removeSelection(selectedTask);
                                    }} />
                                </div>
                            )
                        })}

                    </div>
                ) : null}

                <div className="mt-5">
                    <PrimaryButton
                        
                        disabled={loading}
                        onClick={doUpdateDependency}
                    >
                        Save dependencies
                    </PrimaryButton>
                </div>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<Timeline width={18} height={18} className="fill-text-primary" />}
            />
        </div>
    )
}


const TaskDueButton: React.FC<{task: TaskType}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);

    const [dueMode, setDueMode] = useState<TaskTimeMode>(props.task.time_mode)
    const [date, setDate] = useState<Date>();

    const doUpdateDue = async () => {
        if(dueMode == "start"){
            if(date != undefined){
                await updateTask({start_time: date, end_time: null});
            }
        } else if(dueMode == "end"){
            if(date != undefined){
                await updateTask({start_time: null, end_time: date});
            }
        } else if(dueMode == ""){
            await updateTask({start_time: null, end_time: null});
        }
        
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Due / Start date</h2>

                <TaskDateInput task={props.task} onChange={(mode, date) => {
                    setDueMode(mode);
                    setDate(date);
                }} />

                <div className="mt-5">
                    <PrimaryButton
                        
                        disabled={loading}
                        onClick={doUpdateDue}
                    >
                        Save date
                    </PrimaryButton>
                </div>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<Calendar width={18} height={18} className="fill-text-primary" />}
            />
        </div>
    )
}


const TaskDeleteButton: React.FC<{task: TaskType}> = (props) => {
    const deleteTask = useDeleteTask();
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);

    const doDeleteTask = async () => {
        await deleteTask(props.task);
        setVisible(false);
    }

    return (
        <div>
            <ConfirmModal 
                visible={visible}
                text="Deleting this task will also delete any tasks underneath it, do you want to continue?"
                onConfirm={doDeleteTask}
                onCancel={() => {setVisible(false)}}
            />

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<Trash width={18} height={18} className="fill-text-primary" />}
            />
        </div>
    )
}


const TaskRepeatButton: React.FC<{task: TaskType}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);

    const [repeatMode, setRepeatMode] = useState(props.task.repeat_mode);
    const [repeatAlways, setRepeatAlways] = useState(props.task.repeat_always);
    const [repeatConfig, setRepeatConfig] = useState(props.task.repeat_config);

    const doUpdateRepeat = async () => {
        await updateTask({
            repeat_mode: repeatMode, 
            repeat_config: repeatConfig,
            repeat_always: repeatAlways
        });
        
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Recurring task</h2>

                <TaskRepetitionSelector task={props.task} onSelect={(mode, config) => {
                    setRepeatMode(mode);
                    setRepeatConfig(config);
                }} />

                <InputCheckbox
                    className="mt-5"
                    isChecked={repeatAlways}
                    label={"Should recur even if incomplete"}
                    onUpdateValue={(isChecked) => {
                        setRepeatAlways(isChecked);
                    }}
                />

                <div className="mt-5">
                    <PrimaryButton
                        disabled={loading}
                        onClick={doUpdateRepeat}
                    >
                        Save settings
                    </PrimaryButton>
                </div>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<Repeat width={18} height={18} className={props.task.repeat_mode != 0 ? "fill-brand-primary" : "fill-text-primary"} />}
            />
        </div>
    )
}


const TaskMoveButton: React.FC<{task: TaskType}> = (props) => {
    const updateTask = useTaskUpdater(props.task);
    const [visible, setVisible] = useState(false);
    const loading = useSelector(selectTasksLoading);

    const [workspaceId, setWorkspaceId] = useState(props.task.workspace_id);
    const [parentId, setParentId] = useState(props.task.parent_id);
    const [order, setOrder] = useState<number | undefined>(props.task.order);

    const workspace = useWorkspaceById(workspaceId);
    const parent = useTaskById(parentId);

    const doUpdatePosition = async () => {
        await updateTask({
            workspace: workspace,
            parent: parent,
            order: order
        });
        
        setVisible(false);
    }

    return (
        <div>
            <GenericModal visible={visible} onClose={() => {
                setVisible(false)
            }}>
                <h2 className="mb-5">Move task</h2>

                <TaskPositionSelector task={props.task} onChange={(workspace_id, parent_id, order) => {
                    setWorkspaceId(workspace_id);
                    setParentId(parent_id);
                    setOrder(order);
                }} />

                <div className="mt-5">
                    <PrimaryButton
                        disabled={loading}
                        onClick={doUpdatePosition}
                    >
                        Move task
                    </PrimaryButton>
                </div>

            </GenericModal>

            <PageNavButton
                onClick={() => {setVisible(true)}}
                icon={<MoveInList width={18} height={18} className={"fill-text-primary"} />}
            />
        </div>
    )
}



const TaskShowCompletedToggle: React.FC<{showCompleted: boolean|undefined, setShowCompleted: (show: boolean) => void}> = (props) => {
    const [visible, setVisible] = useState(false);

    const toggle = () => {
        props.setShowCompleted(!props.showCompleted);
    }

    return (
        <div>

            {props.showCompleted ? (
                <PageNavButton
                    onClick={toggle}
                    icon={<Visible 
                        width={18} 
                        height={18} 
                        className={"fill-brand-primary"} 
                    />}
                />
            ) : (
                <PageNavButton
                    onClick={toggle}
                    icon={<Hidden 
                        width={18} 
                        height={18} 
                        className={"fill-text-primary"} 
                    />}
                />
            )}

        </div>
    )
}


