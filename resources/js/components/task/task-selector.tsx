import React, { useState } from "react";
import { Task } from "../../api/task/task"
import { useTaskFinder } from "../../redux/task/hooks"
import { InputText } from "../forms/input"


type TaskSelectorProps = {
    onSelect: (t: Task) => void
}

export const TaskSelector: React.FC<TaskSelectorProps> = (props) => {
    const [criteria, setCriteria] = useState("");
    const found =  useTaskFinder(criteria)

    return (
        <div>

            <div className="relative">
                <InputText
                    label="Search for a task or project"
                    value={criteria}
                    onUpdateValue={setCriteria}
                />
            </div>

            {found.length > 0 ? (
                <div className="border rounded-lg p-3 flex-col gap-5 mt-3">
                    {found.map((task, index) => {
                        return (
                            <div key={"listed_task_"+task.id}>
                                <ListedTask task={task} onClick={() => {
                                    props.onSelect(task);
                                    setCriteria("");
                                }} />
                            </div>
                        )
                    })}
                </div>
            ) : null}

        </div>
    )
}


type ListedTaskProps = {
    task: Task,
    onClick?: () => void
}

export const ListedTask: React.FC<ListedTaskProps> = (props) => {
    return (
        <div onClick={() => {
            if(props.onClick){
                props.onClick();
            }
        }} className="cursor-pointer hover:bg-brand-primary rounded-lg p-2">
            - {props.task.title}
        </div>
    )
}
