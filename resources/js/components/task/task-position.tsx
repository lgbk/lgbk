import React, { useState, useEffect } from "react"
import { InputSelect, InputSelectOption } from "../forms/input"
import { Task } from "../../api/task/task"
import { useParentTaskOf, useWorkspaceTasks, useTaskProject, useChildTasksOf, useTaskById } from "../../redux/task/hooks"
import { useWorkspaces, useWorkspaceById } from "../../redux/workspace/hooks"

type TaskPositionSelectorProps = {
    task: Task,
    onChange: (workspace_id: string, parent_id: string | undefined, order: number | undefined) => void
}

export const TaskPositionSelector: React.FC<TaskPositionSelectorProps> = (props) => {
    const parentProject = useTaskProject(props.task);

    const [workspace, setWorkspace] = useState(props.task.workspace_id);
    const [project, setProject] = useState(parentProject?.id);
    const [position, setPosition] = useState<number | undefined>(props.task.order);
    const [parent, setParent] = useState(props.task.parent_id);

    useEffect(() => {
        props.onChange(workspace, parent, position);
    }, [workspace, parent, position])

    return (
        <div>
            <TaskWorkspaceSelect 
                workspace={workspace} 
                setWorkspace={(val) => {
                    setWorkspace(val);
                    if(val != props.task.workspace_id){
                        setProject(undefined);
                        setPosition(undefined);
                        setParent(undefined);
                    } else {
                        setProject(parentProject?.id);
                        setPosition(props.task.order);
                        setParent(props.task.parent_id);
                    }
                    
                }} 
            />

            <TaskProjectSelect 
                workspace={workspace} 
                project={project} 
                setProject={(val) => {
                    setProject(val);
                    if(val != parentProject?.id){
                        setPosition(undefined);
                        setParent(val);
                    } else {
                        setPosition(props.task.order);
                        setParent(props.task.parent_id);
                    }
                    
                }} 
            />

            <TaskPositionSelect 
                workspace={workspace} 
                project={project} 
                position={position} 
                setPosition={setPosition} 
                parent={parent} 
                setParent={setParent} 
            />
        </div>
    )
}


type TaskWorkspaceSelectProps = {
    workspace: string | undefined,
    setWorkspace: (workspace: string) => void
}

const TaskWorkspaceSelect: React.FC<TaskWorkspaceSelectProps> = (props) => {
    const workspaces = useWorkspaces();

    return (
        <div>

            <InputSelect
                className="mb-3"
                label="Workspace"
                value={props.workspace}
                options={workspaces.map((ws) => {
                    return {value: ws.id, label: ws.title};
                })}
                onUpdateValue={props.setWorkspace}
            />

        </div>
    );
}



type TaskProjectSelectProps = {
    workspace: string | undefined,
    project: string | undefined,
    setProject: (project: string | undefined) => void
}

const TaskProjectSelect: React.FC<TaskProjectSelectProps> = (props) => {
    const workspace = useWorkspaceById(props.workspace);
    const projects = useWorkspaceTasks(workspace);

    const useOptions = projects.map((project) => {
        return {value: project.id, label: project.title};
    });
    useOptions.unshift({value: "", label:"Set as main project"})

    return (
        <div>

            <InputSelect
                className="mb-3"
                label="Within project"
                value={props.project ? props.project : ""}
                options={useOptions}
                onUpdateValue={(val) => {
                    if(val){
                        props.setProject(val);
                    } else {
                        props.setProject(undefined);
                    }
                }}
            />

        </div>
    );
}



type TaskPositionSelectProps = {
    workspace: string | undefined,
    project: string | undefined,
    position: number | undefined,
    setPosition: (position: number) => void
    parent: string | undefined,
    setParent: (task: string | undefined) => void
}

const TaskPositionSelect: React.FC<TaskPositionSelectProps> = (props) => {
    const workspace = useWorkspaceById(props.workspace);
    const project = useTaskById(props.project);

    const workspaceTasks = useWorkspaceTasks(workspace);
    const projectTasks = useChildTasksOf(project, true, true, false);

    let tasks: Task[];
    let useParent: string = "0";
    if(project){
        tasks = projectTasks.filter((task) => task.parent_id == props.project);
        useParent = props.project ? props.project : "0";
    } else {
        tasks = workspaceTasks;
    }

    const getProjectTaskChildren = (t: Task): Task[] => {
        return projectTasks.filter((task) => task.parent_id == t.id);
    }

    const buildHierarchy = (list: Task[], topLevelParentId: string, currentValue: string, includeAppends: boolean = true): InputSelectOption[] => {
        let currentValueInThisList: boolean = false;

        const built = list.map((t) => {
            let thisValue = (t.parent_id ? t.parent_id : "0")+"__"+String(t.order);
            if(thisValue == currentValue){
                currentValueInThisList = true;
            }

            return {
                value: thisValue, 
                label: t.title, 
                children: thisValue != currentValue ? buildHierarchy(getProjectTaskChildren(t), t.id, currentValue, (props.project ? true : false)) : []
            }
        })

        if(includeAppends && !currentValueInThisList){
            built.push({
                value: (!props.project ? "0" : topLevelParentId)+"__"+String(list.length + 1), 
                label: "Append here", 
                children: []
            });
        }

        return built;
    }

    // const useCurrentValue = (props.parent ? props.parent : "0")+"__"+String(props.position);
    let useCurrentValue: string;
    if(props.position){
        useCurrentValue = (props.parent ? props.parent : "0")+"__"+String(props.position);
    } else {
        useCurrentValue = "";
    }

    const useOptions = buildHierarchy(tasks, useParent, useCurrentValue);

    return (
        <div>

            <InputSelect
                className="mb-3"
                label="Task position"
                value={useCurrentValue}
                options={useOptions}
                onUpdateValue={(v) => {
                    const values = v.split("__");
                    const parent = values[0] == "0" ? undefined : values[0] 
                    const order = parseInt(values[1]);

                    props.setParent(parent);
                    props.setPosition(order);
                }}
                showHierarchy={true}
            />

        </div>
    );
}