import React, { useState, useEffect } from "react"
import { InputDate, InputTime, InputType } from "../forms/input"
import { TaskTimeMode, Task } from "../../api/task/task"
import { dateToDateTimeString } from "../../api/api"


type TaskDateInputProps = {
    task: Task,
    onChange: (mode: TaskTimeMode, date?: Date) => void
}

export const TaskDateInput: React.FC<TaskDateInputProps> = (props) => {

    let initialDate = "";
    let initialTime = "";
    let initialDateTime = "";
    if(props.task.time_mode){

        if(props.task.time_mode == "start"){
            if(props.task.start_time){
                initialDateTime = dateToDateTimeString(props.task.start_time);
            }
        } else {
            if(props.task.end_time){
                initialDateTime = dateToDateTimeString(props.task.end_time);
            }
        }

        initialDate = initialDateTime.split(" ")[0];
        initialTime = initialDateTime.split(" ")[1];
    }


    const [date, setDate] = useState(initialDate);
    const [time, setTime] = useState(initialTime);
    const [mode, setMode] = useState<TaskTimeMode>(props.task.time_mode);

    const modeOptions: {label: string, key: TaskTimeMode}[] = [
        //@ts-ignore
        {"label": "None", "key": ""},
        //@ts-ignore
        {"label": "Start at", "key": "start"},
        //@ts-ignore
        {"label": "Due by", "key": "end"},
    ]

    useEffect(() => {
        if(mode == ""){
            props.onChange(mode);
        } else {
            if(date){
                let useDateTime = date;
                if(time != ""){
                    useDateTime = useDateTime+" "+time;
                } else {
                    useDateTime = useDateTime+" 00:00:00";
                }
                let useDateTimeObj = new Date(useDateTime);

                props.onChange(mode, useDateTimeObj);
            }
        }
    }, [mode, date, time])

    return (
        <div>

            <div className="flex flex-row rounded-lg border">
                {modeOptions.map((item, index) => {
                    return (
                        <div 
                            key={"task_date_mode_selector__"+String(index)} 
                            className={"flex justify-center items-center w-1/3 cursor-pointer py-1"+(mode == item.key ? " bg-brand-primary" : "")}
                            onClick={() => {
                                setMode(item.key)
                            }}
                        >
                            {item.label}
                        </div>
                    )
                })}
            </div>

            <div className={"mt-3"+(mode == "" ? " hidden" : "")}>
                <div className="mb-3">
                    <InputDate
                        label={"Date"} 
                        value={date} 
                        onUpdateValue={(date) => {
                            setDate(date);
                        }} 
                    />
                </div>
                
                <InputTime
                    label={"Time"} 
                    value={time} 
                    onUpdateValue={(time) => {
                        setTime(time);
                    }} 
                />
            </div>
            
        </div>
    )
}