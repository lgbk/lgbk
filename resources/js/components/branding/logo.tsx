import React from "react";

export const TextLogo: React.FC = () => {
    return (
        <div className="text-white font-bold" style={{height: "24px", fontSize: "30px", lineHeight: "24px"}}>
            LGBK
        </div>
    )
}


export const LargeTextLogo: React.FC = () => {
    return (
        <div className="text-white font-bold" style={{height: "100px", fontSize: "90px", lineHeight: "90px"}}>
            LGBK
        </div>
    )
}