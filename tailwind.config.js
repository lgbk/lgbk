/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.tsx",
    "./resources/**/**/*.tsx",
    "./resources/**/**/**/*.tsx",
  ],
  theme: {
    screens: {
      sm: '480px',
      md: '768px',
      lg: '976px',
      xl: '1440px',
    },
    extend: {
      colors: {
        'brand-primary': '#5fd6ff',
        'brand-secondary': '#f39237',

        'text-primary': '#4f646f',

        'main-background': '#ffffff',
        'side-background': '#ebfaff',
        'side-background-detail': '#e0f6fd',

        'priority-very-high' : '#b94f42',
        'priority-high' : '#d68d36',
        'priority-normal' : '#5fd6ff',
        'priority-low' : '#37d13b',
        'priority-very-low' : '#599c5b',
      },
    },
  },
  plugins: [],
}

