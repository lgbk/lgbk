<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix("auth")->group(function(){
    Route::get('unauthenticated', \App\Http\Controllers\Auth\AuthController::class . '@unauthenticated')->name("login");
    Route::post('login', \App\Http\Controllers\Auth\AuthController::class . '@login');
    Route::post('refresh', \App\Http\Controllers\Auth\AuthController::class . '@refresh');
});

Route::post('user/register', \App\Http\Controllers\User\UserController::class . '@register');

Route::middleware('auth:api')->group(function(){

    Route::prefix("auth")->group(function(){
        Route::post('logout', \App\Http\Controllers\Auth\AuthController::class . '@logout');
    });

    Route::prefix("user")->group(function(){
        Route::get('//', \App\Http\Controllers\User\UserController::class . '@getUser');
        Route::patch('password', \App\Http\Controllers\User\UserController::class . '@changePassword');
    });

    Route::prefix('workspace')->group(function(){
        Route::post('//', \App\Http\Controllers\Tasks\WorkspaceController::class . '@createWorkspace');
        Route::get('//', \App\Http\Controllers\Tasks\WorkspaceController::class . '@listWorkspaces');
        Route::patch('/{id}', \App\Http\Controllers\Tasks\WorkspaceController::class . '@updateWorkspace');
        Route::patch('/{id}/reorder', \App\Http\Controllers\Tasks\WorkspaceController::class . '@reorderWorkspace');
        Route::delete('/{id}', \App\Http\Controllers\Tasks\WorkspaceController::class . '@deleteWorkspace');
    });

    Route::prefix('task')->group(function(){
        Route::post('//', \App\Http\Controllers\Tasks\TaskController::class . '@createTask');
        Route::get('/next', \App\Http\Controllers\Tasks\TaskController::class . '@getNextTasks');
        Route::get('/{task_id?}', \App\Http\Controllers\Tasks\TaskController::class . '@getTask');
        Route::patch('/{task_id}', \App\Http\Controllers\Tasks\TaskController::class . '@updateTask');
        Route::delete('/{task_id}', \App\Http\Controllers\Tasks\TaskController::class . '@deleteTask');
        Route::post('/{task_id}/dependency', \App\Http\Controllers\Tasks\TaskController::class . '@addDependencies');
        Route::delete('/{task_id}/dependency', \App\Http\Controllers\Tasks\TaskController::class . '@deleteDependencies');
        Route::put('/{task_id}/dependency', \App\Http\Controllers\Tasks\TaskController::class . '@updateAllDependencies');
    });

});


