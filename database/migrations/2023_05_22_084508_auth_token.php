<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth_tokens', function (Blueprint $table) {
            $table->uuid("id")->unique();

            $table->uuid('user_id');
            $table->uuid('sibling_id')->nullable();

            $table->enum('type', ['auth', 'refresh']);
            
            $table->text('token');
            $table->string('token_hash', 64)->unique();

            $table->datetime('expires');
            $table->boolean('used')->default(false);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('auth_tokens', function (Blueprint $table) {
            $table->foreign('sibling_id')->references('id')->on('auth_tokens');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth_tokens');
    }
};
