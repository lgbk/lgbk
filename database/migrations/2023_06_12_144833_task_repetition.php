<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tasks', function (Blueprint $table) {
            //0 - off, 1 - weekdays, 2 - month days, 3 - yearly months
            $table->enum('repeat_mode', [0, 1, 2, 3])->default(0);
            $table->boolean('repeat_always')->default(false);
            $table->json('repeat_config')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tasks', function (Blueprint $table) {
            $table->dropColumn('repeat_mode');
            $table->dropColumn('repeat_always');
            $table->dropColumn('repeat_config');
        });
    }
};
