<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->uuid("id")->unique();

            $table->text('title');
            $table->enum('priority', [1, 2, 3, 4, 5])->default(3);
            $table->smallInteger('order');
            $table->smallInteger('estimate')->nullable();
            $table->boolean('complete')->default(false);

            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();

            $table->uuid('workspace_id');
            $table->uuid('parent_id')->nullable();

            $table->timestamps();

            $table->foreign('workspace_id')->references('id')->on('workspaces')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('tasks', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('tasks')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('task_dependencies', function (Blueprint $table) {
            $table->uuid("task");
            $table->uuid("requires");
            
            $table->foreign('task')->references('id')->on('tasks')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('requires')->references('id')->on('tasks')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('task_dependencies');
        Schema::dropIfExists('tasks');
    }
};
